<?php if ( !empty( $user ) ) {
	foreach ( $user as $key=>$value ) {
		$image	=	(!empty($value['photo'])? base_url().'assets/upload_images/'.$value['photo']:base_url().'assets/upload_images/'.'default.jpeg');
		$photo	=	(!empty($value['photo'])? $value['photo']:'');
		$uname	=	(!empty( $value['username'] ) ? $value['username'] : '');
		$email	=	(!empty( $value['email'] ) ? $value['email'] : '');
		$phone	=	(!empty( $value['phone'] ) ? $value['phone'] : '');
		$userGuid	=	(!empty( $value['userGuid'] ) ? $value['userGuid'] : '');
		$centername	=	(!empty( $value['center_name'] ) ? $value['center_name'] : '');
		$weburl		=	(!empty( $value['web_url'] ) ? $value['web_url'] : '');
		$packages	=	(!empty( $value['packages'] ) ? $value['packages'] : '');
		$offers		=	(!empty( $value['offers'] ) ? $value['offers'] : '');
		$valueAdded	=	(!empty( $value['value_added_service'] ) ? $value['value_added_service'] : '');
		$gender		=	(!empty( $value['gender'] ) ? $value['gender'] : '');
		$address	=	(!empty( $value['address'] ) ? $value['address'] : '');
	}
}
?>
<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script>
<style>
.table {
    margin-top: 20px
}
</style>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12 page-header">
                <h4>
                 	<?php if ( !empty( $menu ) && !empty( $submenu )) { ?>
                 		<a href ="<?php echo $menu['url']?>"><?php echo $menu['name']?></a> / 
                 		<a href ="<?php echo $submenu['url']?>"><?php echo $submenu['name']?></a> /
                 		<?php } ?>
                 		<a href ="<?php echo base_url().'/index.php/customercare/complaints?cid='.$cid.'&sid='.$sid?>">Complaints</a>
                 	</h4>
                    <h1 class="col-md-6">Complaints</h1>
                    <h1 class="col-md-6 pull-right"> Center Details </h1>
                </div>
                <!-- /.col-lg-12 -->
                <?php if ( !empty( $message ) ) { ?>
  					<script>
						$(function () {
							 $('#success_msg').show();
								setTimeout(function() {
						 			 $('#success_msg').hide();
								}, 3000);
						});
					</script>
					<div class="msg success-msg" id="success_msg" style="text-align: center;"><?php echo $message;?></div>
			<?php }?>
            </div>
            <!-- /.row -->
            <div class="row">
                 <div class="col-lg-12">
                        <div class="panel-body">
                        <div class="row">
						<div class="container">
					      <div class="row">
					            <div class="panel-body">
					              <div class="row">
					                <div class="col-md-6"> 
									  <?php 
									  $url =  base_url().'index.php/customercare/addcomplaints?cid='.$cid.'&sid='.$sid;
									  echo form_open_multipart($url, array('id' => 'myform'));?>
								     		 <fieldset class="form-group">
								                <label for="Username">Caller Name <label class="red">*</label></label>
								                <input type="text" placeholder="Caller Name" maxlength="120" data-validation="alphanumeric" data-validation-error-msg="Please Enter valid Username" data-validation-allowing="-,_ " data-validation-optional="false" class="form-control" name="caller_name" id="caller_name" value="<?php echo (!empty($customer['name'])?$customer['name']:'')?>">
								              </fieldset>
								              <fieldset class="form-group">
								                <label for="phone">Phone/Mobile Number <label class="red">*</label></label>
								                <input type="text" placeholder="Phone/Mobile Number" maxlength="15" data-validation-error-msg="Enter a valid number" data-validation="required" data-validation-optional="false" class="form-control" name="mobile" id="mobile" value="<?php echo (!empty($customer['mobile'])?$customer['mobile']:'')?>">
								              </fieldset>
											<fieldset class="form-group">
											  <label for=""address"">Address</label>
											  <textarea name="address" id="address" class="form-control"><?php echo (!empty($customer['address'])?$customer['address']:'')?></textarea>
											</fieldset>
											<fieldset class="form-group">
											  <label for="concern">Feedback / Suggestions /Gym details & facilities <label class="red">*</label></label>
											  <textarea name="concern" id="concern" class="form-control" data-validation="required" data-validation-error-msg="Please Enter Feedback details"  data-validation-optional="false"></textarea>
											</fieldset>
											<fieldset class="form-group">
											     <label>Center name <label class="red">*</label></label>
											     <?php echo form_dropdown('centername', $centerNameList, $centerId, 'class="form-control" id="centername"  data-validation="required" data-validation-error-msg="Please select any one"');?>
											 </fieldset>
											 <input type="hidden" name="customerId" value="<?php echo (!empty($customer['userGuid'])?$customer['userGuid']:'')?>">
										 <fieldset class="form-group">
								        	<button type="submit" class="btn btn-primary pull-right">Save Changes</button>
								    	</fieldset>
								    </div>
								    	
								    </form>
								    <?php if (!empty( $user )) {?>
								    <div id="centerProfile">
								    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="<?php echo $image;?>" class="img-responsive" height="150" width="150"> </div>
							          <div class="col-lg-6"> 
			                            <table class="table user">  
							            <tr>
							              <th>Center Name</th><td><?php echo $centername;?></td>
							            </tr>
							            <tr>
							              <th>User Name</th><td><?php echo $uname;?></td>
							            </tr>
							            <tr>
							              <th>Email</th><td><?php echo $email;?></td>
							            </tr>
							            <tr>
							              <th>Mobile  </th><td><?php echo $phone;?></td>
							            </tr>
							            <tr>
							              <th>Gender  </th><td><?php echo $gender;?></td>
							            </tr>
							            <tr>
							              <th>Address  </th><td><?php echo $address;?></td>
							            </tr>
							            <tr>
							              <th>Web URL  </th><td><?php echo $weburl;?></td>
							            </tr>
							            <tr>
							              <th>Pakages  </th><td><?php echo $packages;?></td>
							            </tr>
							            <tr>
							              <th>Offers  </th><td><?php echo $offers;?></td>
							            </tr>
							            <tr>
							              <th>Value Added  </th><td><?php echo $valueAdded;?></td>
							            </tr>
							          </table>
						          </div>
						          </div>
		    					 <?php }?>
		    					 </div>
		    				</div>
					     </div>
					  </div>
					</div>
				</div>
			</div>
		 </div><!-- regform -->
      </div>

<script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
<script src="<?php echo base_url();?>assets/js/form-validator/security.js"></script>
<script type="text/javascript">


$.validate({
	  onError : function() {
			 $(":input.error:first").focus();
			 return false;
		    },
    onValidate : function() {
	    }
	});
	
function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#blah')
                   .attr('src', e.target.result)
                   .width(100)
                   .height(100);
           };

           reader.readAsDataURL(input.files[0]);
       }
   }

$('#centername').blur(function(){
	var reqData = $(this).val();
	if(reqData == 0){
		return false
	}
	var baseurl = $('#baseurl').val();
	$.ajax({
        type:"post",
        dataType:"json",
        url: baseurl+'index.php/customercare/getcenternameprfile/'+reqData,
        success: function(response) {
            $('#centerProfile').html(response.data);
        },
        error: function(data) {
            
        },
    });

    return false;
});
</script>


