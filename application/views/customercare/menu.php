<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                	<h4><a href ="<?php echo $url; ?>"><?php echo $title; ?></a> </h4>
                    <h1 class="page-header"><?php echo $title; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            <?php if ( !empty( $menu)) {
            		foreach ( $menu as $key => $value ){
            			$roleId = (!empty( $value['role_id'] ) ? $value['role_id']:'');
            ?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel <?php echo $color;?>">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="<?php echo $icon;?>"></i>
                                </div>
                                <div class="col-xs-9 text-right	">
                                    <div class="huge"></div>
                                    <div><?php echo $value['name'];?></div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url().'index.php/customercare/search/'.$value['id'].'/'.$roleId;?>">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php }}else{ echo'<h4> Details not found</h4>';}?>
            </div>
            <!-- /.row -->
            </div>
