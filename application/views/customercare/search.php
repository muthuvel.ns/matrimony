<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                 	<h4>
                 	<?php if ( !empty( $menu ) && !empty( $submenu )) { ?>
                 		<a href ="<?php echo $menu['url']?>"><?php echo $menu['name']?></a> / 
                 		<a href ="<?php echo $submenu['url']?>"><?php echo $submenu['name']?></a>
                 		<?php } ?>
                 	</h4>
                    <h1 class="page-header">Search</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lists
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            <?php  if(!empty( $fitness )){?>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Center Name</th>
                                           <!--  <th>Customers</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $i=1;
                                    foreach ( $fitness as $key => $value ) { 
                                    		$data = '';
                                    		if ( !empty( $value['customerId'])){
                                    			$data = '&uid='.$value['customerId'];
                                    		}
                                    	?>
                                        <tr class="odd gradeX">
                                            <td>
                                            	<table>
					                        		<tbody>
					                                    <tr>
					                                       <td>Center Name</td>
					                                       <td><b>&nbsp; : &nbsp;</b></td>
					                                       <td><a href="<?php echo base_url().'index.php/customercare/complaints?cid='.$value['centerId'].'&sid='.$subMenuId;?>"><?php echo $value['center_name']?></a></td>
					                                    </tr>
					                                     <tr>
					                                       <td>User Name</td>
					                                       <td><b>&nbsp; : &nbsp;</b></td>
					                                       <td><?php echo $value['username']?></td>
					                                    </tr>
					                                    <tr>
					                                       <td>Web Link</td>
					                                       <td><b>&nbsp; : &nbsp;</b></td>
					                                       <td><?php echo $value['web_url']?></td>
					                                    </tr>
					                                </tbody>
					                        	</table>
                                            </td>
                                           <!-- <td><b>Member Name : </b> <a href="<?php //echo base_url().'index.php/customercare/complaints?cid='.$value['centerId'].'&sid='.$subMenuId.$data?>"><?php //echo (!empty( $value['customername'] )?$value['customername']:'None')?></a></td>
                                           --> 
                                        </tr>
                                        <?php $i++;}
            							?>
                                    </tbody>
                                </table>
                                <?php } else { echo '<center><h4>Details Not Found</h4></center>';}?>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
