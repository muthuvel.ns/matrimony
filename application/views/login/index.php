<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

   <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css';?>" rel="stylesheet">

	<!-- MetisMenu CSS -->
    <link href="<?php echo base_url().'assets/css/metisMenu/metisMenu.min.css';?>" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/admin.css';?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url().'assets/css/font-awesome/css/font-awesome.min.css';?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Login</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="" >
                        <?php if ( !empty( $message ) ) { ?>
  <script>
						$(function () {
							 $('#success_msg').show();
								setTimeout(function() {
						 			 $('#success_msg').hide();
								}, 3000);
						});
					</script>
					<div class="msg error-msg" id="success_msg"><?php echo $message;?></div>
			<?php }?>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" type="email" autofocus name="username" id="login" data-validation-optional= "false" data-validation="email" data-validation-error-msg="Please enter the valid email" value="" >
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password"  type="password" name="password" id="password" data-validation="required" data-validation-error-msg="Please enter the password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery-2.1.4.min.js';?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js';?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url().'assets/js/metisMenu/metisMenu.min.js';?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url().'assets/js/admin.js';?>"></script>

</body>
<script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
<script>
// Setup form validation
$.validate({
	 onError : function() {
		 $(":input.error:first").focus();
		 return false;
	    },
});
</script>
</html>
