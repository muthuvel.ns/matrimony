<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                 	<h4>
                 	<?php  if ( !empty( $menu ) ) { ?>
                 		<a href ="<?php echo $menu['url']?>"><?php echo $menu['name']?></a> 
                 	<?php } if ( !empty( $submenu ) ) {?>
                 		/ <a href ="<?php echo $submenu['url']?>"><?php echo $submenu['name']?></a>
                 	<?php } ?>
                 	</h4>
                    <h1 class="page-header">Date Wise Complaints</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><?php echo ucwords( $customercare_name );?> Taken Complaint List</h4>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            <?php  if(!empty( $complaints )){?>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Complaint Date</th>
                                            <th>Complaint Count</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $i=1;
                                    foreach ( $complaints as $key => $value ) { 
                                    	?>
                                        <tr class="odd gradeX">
                                            <td>
                                            	<a title="Date wise complaints" href="<?php echo $url.$value['userGuid'].'&id='.strtotime($value['date']);?>"><?php echo $value['date']?></a>
                                            </td>
                                            <td> <?php echo $value['counts']?></td>
                                        </tr>
                                        <?php $i++;}
            							?>
                                    </tbody>
                                </table>
                                <?php } else { echo '<center><h4>Details Not Found</h4></center>';}?>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
