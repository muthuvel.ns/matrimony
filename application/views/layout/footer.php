<div class="panel-footer">
                        </div>
                        <!-- /.panel-footer -->
                    </div>
                    <!-- /.panel .chat-panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
     <script src="<?php echo base_url().'assets/js/jquery-2.1.4.min.js';?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js';?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url().'assets/js/metisMenu/metisMenu.min.js';?>"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url();?>assets/js/data-table/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/data-table/dataTables.responsive.js"></script>
    
    <!-- Custom Theme JavaScript -->
     <script src="<?php echo base_url().'assets/js/admin.js';?>"></script>

 <script src="<?php echo base_url();?>assets/js/jquery.pajinate.js"></script>
 <script>
$(document).ready(function(){
	$('#paging_container8').pajinate({
		num_page_links_to_display : 3,
		items_per_page : 4	
	});
	$('#dataTables-example').DataTable({
		responsive: true
    });
});
    </script>
</body>

</html>
