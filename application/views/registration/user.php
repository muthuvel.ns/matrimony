<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/register.js"></script>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Member Registration ( GYM )</h1>
                </div>
                <!-- /.col-lg-12 -->
                <?php if ( !empty( $message ) ) { ?>
  					<script>
						$(function () {
							 $('#success_msg').show();
								setTimeout(function() {
						 			 $('#success_msg').hide();
								}, 3000);
						});
					</script>
					<div class="msg error-msg" id="success_msg" style="text-align: center;"><?php echo $message;?></div>
			<?php }?>
            </div>
            <!-- /.row -->
            <div class="row">
                 <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User details 
                        </div>
                        <div class="panel-body">
                            <div class="row">
  <?php 
  $url = base_url().'index.php/registration/adduser';
  echo form_open_multipart($url, array('id' => 'myform'));?>
   <div class="col-lg-6">
     		 <fieldset class="form-group">
                <label for="Username">Name <label class="red">*</label></label>
                <input type="text" placeholder="User Name" maxlength="120" data-validation="alphanumeric" data-validation-error-msg="Please Enter valid  name" data-validation-allowing="-,_ " data-validation-optional="false" class="form-control" name="username" id="username" value="">
              </fieldset>
			
			<div id="emailid">
              <fieldset class="form-group">
                <label for="email">Email <label class="red">*</label></label>
                <input type="text" placeholder="Email" onblur="customeremailavailablility()" focus="" data-validation-error-msg="Please Enter valid e-mail" data-validation-length="max72" data-validation="email" data-validation-optional="false" class="form-control" name="email" id="email" value="">
              </fieldset>
			</div>
              <fieldset class="form-group">
                <label for="Password">Password <label class="red">*</label></label>
                <input type="password" placeholder="Password" data-validation-length="min6" data-validation-error-msg="You can not leave this field as empty" data-validation="length" data-validation-optional="false" class="form-control" name="pass_confirmation" id="pass_confirmation" value="">
              </fieldset>

              <fieldset class="form-group">
                <label for="repassword">Confirm Password <label class="red">*</label></label>
                <input type="password" placeholder="Confirm Password" data-validation-error-msg="Password are mismatch" data-validation="confirmation" data-validation-optional="false" class="form-control" name="pass" id="rpass" value="">
              </fieldset>

              <fieldset class="form-group">
                <label for="phone">Phone/Mobile Number <label class="red">*</label></label>
                <input type="text" placeholder="Phone/Mobile Number" maxlength="15" data-validation-error-msg="Enter a valid number" data-validation="required" data-validation-optional="false" class="form-control" name="phone" id="phone" value="">
              </fieldset>
              
              <fieldset class="form-group">
			        <label for="phone">Gender</label>
			            <div class="btn-group" data-toggle="buttons">
			                <label class="btn btn-default">
			                    <input type="radio" name="gender" value="male" /> Male
			                </label>
			                <label class="btn btn-default">
			                    <input type="radio" name="gender" value="female" /> Female
			                </label>
			                <label class="btn btn-default active">
			                    <input type="radio" name="gender" value="other" /> Other
			                </label>
			            </div>
		    	</fieldset>
		    	<fieldset class="form-group">
	                <label for="address">Address</label>
	                <textarea class="form-control" name="address" id="address"></textarea>
              	</fieldset>
              
				<fieldset class="form-group">
				     <label>Center name ( Gym )<label class="red">*</label></label>
				     <?php echo form_dropdown('centername', $centerNameList, '', 'class="form-control" data-validation="required" data-validation-error-msg="Please select any one"');?>
				     <!-- <select class="form-control" data-validation="required" data-validation-error-msg="Please select any one">
				     	<option value="">Select center name</option>
				         <option>1</option>
				         <option>2</option>
				         <option>3</option>
				         <option>4</option>
				         <option>5</option>
				     </select> -->
				 </fieldset>
				<fieldset class="form-group">
	                <label for="photo">Image</label>
	                <div><span style="float:left;"><img alt="" src="#" id="blah"></span></div>
	                 <input type="file" name="photo" id="photo" onchange="readURL(this);" class="upload" accept="image/*"/>
	            </fieldset>

              <fieldset class="form-group">
              <button type="submit" class="btn btn-primary pull-right">Registration</button>
              </fieldset>
    </div>
    </form>
    </div>
    </div>
    </div><!-- regform -->
            </div>
            <!-- /.row -->
            </div>

<script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
<script src="<?php echo base_url();?>assets/js/form-validator/security.js"></script>
<script>
// just for the demos, avoids form submit

/*$( "#myform" ).validate({
  rules: {
    password: "required",
    repassword: {
      equalTo: "#password"
    }
  }
});*/

	

$.validate({
	  modules : 'security',
	  onError : function() {
			 $(":input.error:first").focus();
			 return false;
		    },
    onValidate : function() {
		 errortext	=	$("#email").attr('current-error');
		 if(errortext!='') {
			  return {
		        element : $('#email'),
		        message : errortext,
		      }
		 }
		 
	    },
	});
	
function readURL(input) {
	var ext = $('#photo').val().split('.').pop().toLowerCase();
	if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		$('#photo').val('');
		 $('#blah').attr('src', '');
// 		$("#photo" ).attr( "current-error", "Invalid File Format.Allows Only Image File!" );
// 		$('#photo').removeClass('valid').addClass('error');
// 		$("#image").removeClass('has-success').addClass('has-error');
// 		$('#photo').html('<p class="msg error-msg">Invalid File Format.Allows Only Image File</p>');
// 		$('.msg .error-msg').show();
	   	alert('Invalid File Format.Allows Only Image File ');
	    return false;
	}
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	$('#blah')
            .attr('src', e.target.result)
            .width(100)
            .height(100);
        }
        reader.readAsDataURL(input.files[0]);
    } 
}
   
</script>
