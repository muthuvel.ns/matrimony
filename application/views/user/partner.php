<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Fitness Centers</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lists
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            <?php if(!empty( $userlist )){?>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Fitness Center Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $i=1;
                                    foreach ( $userlist as $key => $value ) { ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $value['center_name']?></td>
                                            <td><?php echo $value['phone']?></td>
                                            <td><?php echo $value['email']?></td>
                                            <td>
                                            	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            	<a href="<?php echo base_url().'index.php/user/profile/'.$value['userGuid'].'/'.PARTNER_ROLE_ID;?>" id="edit" title="Edit"> <i class="fa fa-edit"></i></a>
                                            	&nbsp;&nbsp;<a href="<?php echo base_url().'index.php/user/disable?uid='.$value['userGuid'].'&rid='.PARTNER_ROLE_ID;;?>" id="delete" title="Delete"> <i class="fa fa-bitbucket"></i></a>
                                            	&nbsp;&nbsp;<a href="<?php echo base_url().'index.php/home/complaintsdetails?pid='.$value['userGuid'];?>" id="complaints" title="Complaints"> <i class="fa fa-comment"></i></a>
                                            </td>
                                        </tr>
                                        <?php 	$i++;}
            							?>
                                    </tbody>
                                </table>
                                <?php } else { echo '<center><h4>Details Not Found</h4></center>';}?>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
