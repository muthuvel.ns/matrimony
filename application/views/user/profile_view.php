<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                 	<h4>
                 	<?php  if ( !empty( $menu ) ) { ?>
                 		<a href ="<?php echo $menu['url']?>"><?php echo $menu['name']?></a> 
                 	<?php } if ( !empty( $submenu ) ) {?>
                 		/ <a href ="<?php echo $submenu['url']?>" id="sub"><?php echo $submenu['name']?></a>
                 	<?php } ?>
                 	</h4>
                    <h1 class="page-header" id="edit_profile">Profile</h1>
                </div>
                <!-- /.col-lg-12 -->
                <?php if ( !empty( $message ) ) { ?>
  					<script>
						$(function () {
							 $('#success_msg').show();
								setTimeout(function() {
						 			 $('#success_msg').hide();
								}, 3000);
						});
					</script>
					<div class="msg error-msg" id="success_msg" style="text-align: center;"><?php echo $message;?></div>
			<?php }?>
            </div>
            <!-- /.row -->
            <div class="row">
                 <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <?php if ( $roleguid == PARTNER_ROLE_ID) {?>
                            Fitness center details 
                        <?php   }?>
                             Customer Care
                             
                        </div>
                        <div class="panel-body">
                            <div class="row">
                           		<?php if ( !empty( $user ) ) {
										foreach ( $user as $key=>$value ) {
											$image	=	(!empty($value['photo'])? base_url().'assets/upload_images/'.$value['photo']:base_url().'assets/images/'.'co.png');
											$photo	=	(!empty($value['photo'])? $value['photo']:'');
											$uname	=	(!empty( $value['username'] ) ? $value['username'] : '');
											$email	=	(!empty( $value['email'] ) ? $value['email'] : '');
											$phone	=	(!empty( $value['phone'] ) ? $value['phone'] : '');
											$userGuid	=	(!empty( $value['userGuid'] ) ? $value['userGuid'] : '');
											$centername	=	(!empty( $value['center_name'] ) ? $value['center_name'] : '');
											$weburl		=	(!empty( $value['web_url'] ) ? $value['web_url'] : '');
											$packages	=	(!empty( $value['packages'] ) ? $value['packages'] : '');
											$offers		=	(!empty( $value['offers'] ) ? $value['offers'] : '');
											$valueAdded	=	(!empty( $value['value_added_service'] ) ? $value['value_added_service'] : '');
											$gender		=	(!empty( $value['gender'] ) ? $value['gender'] : '');
											$address	=	(!empty( $value['address'] ) ? $value['address'] : '');
										}
									?>
								<div id="profile">
		                           	<div class="col-md-3 col-lg-3 " align="center"> 
		                           		<img alt="User Pic" src="<?php echo $image;?>" class="img-responsive" height="150" width="150">
		                           		<div class="btn">
		                           			<button class="btn btn-primary" id="edit_btn">Edit Profile</button>
		                           		</div>
		                           	 </div>
							          <div class="col-lg-6"> 
			                            <table class="table user">  
			                            <?php if ( $roleguid == PARTNER_ROLE_ID) {?> 
							            <tr>
							              <th>Center Name</th><td><?php echo $centername;?></td>
							            </tr>
							            <?php } ?>
							            <tr>
							              <th>User Name</th><td><?php echo $uname;?></td>
							            </tr>
							            <tr>
							              <th>Email</th><td><?php echo $email;?></td>
							            </tr>
							            <tr>
							              <th>Mobile  </th><td><?php echo $phone;?></td>
							            </tr>
							            <tr>
							              <th>Gender  </th><td><?php echo $gender;?></td>
							            </tr>
							            <tr>
							              <th>Address  </th><td><?php echo $address;?></td>
							            </tr>
							            <?php if ( $roleguid == PARTNER_ROLE_ID) {?>
							            <tr>
							              <th>Web URL  </th><td><?php echo $weburl;?></td>
							            </tr>
							            <tr>
							              <th>Pakages  </th><td><?php echo $packages;?></td>
							            </tr>
							            <tr>
							              <th>Offers  </th><td><?php echo $offers;?></td>
							            </tr>
							            <tr>
							              <th>Value Added  </th><td><?php echo $valueAdded;?></td>
							            </tr>
							            <?php } ?>
							          </table>
						          </div>
					          </div>
					<div id="edit" style="display: none;">
			  <?php 
			  $url = base_url().'index.php/user/editprofile/'.$userGuid.'/'.$roleguid;
			  echo form_open_multipart($url, array('id' => 'myform'));?>
		  		 <div class="col-lg-6">
		     		 <fieldset class="form-group">
		                <label for="Username">Name</label>
		                <input type="text" placeholder="User Name" maxlength="120" data-validation="alphanumeric" data-validation-error-msg="Please Enter valid Username" data-validation-allowing="-,_ " data-validation-optional="false" class="form-control" name="username" id="username" value="<?php echo $uname;?>">
		              </fieldset>
					<div id="emailid">
		              <fieldset class="form-group">
		                <label for="email">Email</label>
		                <input type="text" readonly placeholder="Email" onblur="emailavailablility()" focus="" data-validation-error-msg="Please Enter valid e-mail" data-validation-length="max72" data-validation="email" data-validation-optional="false" class="form-control" name="email" id="email" value="<?php echo $email;?>">
		              </fieldset>
					</div>
						<fieldset class="form-group">
		                <label for="repassword">Password <a id="flip">(click)</a></label>
		              </fieldset>
		              <div id="pwd_edit" style="display: none">
		              <fieldset class="form-group">
		                <label for="Password">Password</label>
		                <input type="password" autocomplete="off" placeholder="Password" data-validation-length="min6" data-validation-error-msg="You can enter min 6 digits" data-validation="length" data-validation-optional="true" class="form-control" name="pass_confirmation" id="pass_confirmation" value="">
		              </fieldset>
		
		              <fieldset class="form-group">
		                <label for="repassword">Confirm Password</label>
		                <input type="password" autocomplete="off" placeholder="Confirm Password" data-validation-error-msg="Password are mismatch" data-validation="confirmation" data-validation-optional="true" class="form-control" name="pass" id="rpass" value="">
		              </fieldset>
		              </div>
		              <fieldset class="form-group">
		                <label for="phone">Phone/Mobile Number</label>
		                <input type="text" placeholder="Phone/Mobile Number" maxlength="15" data-validation-error-msg="Enter a valid number" data-validation="required" data-validation-optional="false" class="form-control" name="phone" id="phone" value="<?php echo $phone;?>">
		              </fieldset>
		              
		              <?php 
		              $male='';
		              $female='';
		              $others='';
		              $ch_male = '';
		              $ch_female = '';
		              if ( $gender == 'male') { $male = 'active'; $ch_male = 'checked=""'; 
		              }elseif ($gender == 'female'){ $female = 'active'; $ch_female = 'checked=""'; 
		              }else{$others == 'active';}
		              ?>
						<fieldset class="form-group">
					        <label class="control-label">Gender</label>
					        <div class="">
					            <div class="btn-group" data-toggle="buttons">
					                <label class="btn btn-default <?php echo $male?>">
					                    <input type="radio" <?php echo $ch_male?> name="gender" value="male" /> Male
					                </label>
					                <label class="btn btn-default <?php echo $female?>">
					                    <input type="radio" <?php echo $ch_female?> name="gender" value="female" /> Female
					                </label>
					                <label class="btn btn-default <?php echo $others?>">
					                    <input type="radio" name="gender" value="other" /> Other
					                </label>
					            </div>
					        </div>
				    	</fieldset>
					<fieldset class="form-group">
					  <label for=""address"">Address</label>
					  <textarea name="address" id=""address"" class="form-control"><?php echo $address;?></textarea>
					</fieldset>
					 <fieldset class="form-group">
		                <label for="photo">Photo</label>
		                <div><span style="float:left;"><img alt="" src="<?php echo $image ?>" id="blah" width="100" height="100"></span></div>
		                 <div class="fileUpload">
		                 <input type="file" name="photo" id="photo" onchange="readURL(this);" class="upload" value="<?php echo $photo;?>">
		                </div>
		              </fieldset>
		    </div>
		    <?php if ( $roleguid == PARTNER_ROLE_ID) {?>
		    <div class="col-lg-6">
				 <fieldset class="form-group">
		            <label for="centername">Gym/ Fitness Center Name </label>
		            <input type="text" placeholder="Center Name" maxlength="120" data-validation="required" data-validation-error-msg="Please Enter valid Centername" data-validation-optional="false" class="form-control" name="centername" id="centername" value="<?php echo $centername;?>">
		         </fieldset>
				<fieldset class="form-group">
				  <label for="Username">website Url</label>
				  <input type="text" placeholder="Website Url"  data-validation="required" data-validation-error-msg="Please Enter valid website url"  data-validation-optional="false" class="form-control" name="web_url" id="web_url" value="<?php echo $weburl;?>">
				</fieldset>
				<fieldset class="form-group">
				  <label for="Username">Packages</label>
				  <textarea name="package" id="package" class="form-control"><?php echo $packages;?></textarea>
				</fieldset>
				<fieldset class="form-group">
				  <label for="Username">Special packages/ offers</label>
				  <textarea name="offer" id="offer" class="form-control"><?php echo $offers;?></textarea>
				</fieldset>
				<fieldset class="form-group">
				  <label for="Username">Any other value added services</label>
				  <textarea name="value_added_service" id="value_added_service" class="form-control"><?php echo $valueAdded;?></textarea>
				</fieldset>
		    </div>
		    <?php } ?>
		    <div class="col-lg-12">
		    	<div class="col-lg-6">
			    	<fieldset class="form-group">
			        	<button type="submit" class="btn btn-primary pull-right">Save Changes</button>
			    	</fieldset>
		    	</div>
		    </div>
		    </form>
		    </div>
		    <?php  
		} else {?><h2>Details Not Found</h2><?php } ?>
			</div>
		    </div>
		    </div><!-- regform -->
            </div>
            </div>
            <!-- /.row -->
            </div>

<script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
<script src="<?php echo base_url();?>assets/js/form-validator/security.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#flip").click(function() {
		$("#pwd_edit").slideToggle("slow");
	});
});

$( "#edit_btn" ).click(function() {
	$('#profile').hide();
	$('#edit').show();
	$('#edit_btn').hide();
	$('#edit_profile').html('Edit Profile');
	$('#rpass').val('');
	$('#pass_confirmation').val('');
	$('#sub').html('Edit Profile');
});


$.validate({
	  modules : 'security',
	  onError : function() {
			 $(":input.error:first").focus();
			 $("#pwd_edit").slideToggle("slow");
			 return false;
		    },
    onValidate : function() {
		 errortext	=	$("#email").attr('current-error');
		 if(errortext!='') {
			  return {
		        element : $('#email'),
		        message : errortext,
		      }
		 }
	    }
	});
	
function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#blah')
                   .attr('src', e.target.result)
                   .width(100)
                   .height(100);
           };

           reader.readAsDataURL(input.files[0]);
       }
   }
</script>


