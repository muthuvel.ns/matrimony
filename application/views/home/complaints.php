<div id="page-wrapper">
			<div class="row">
                <div class="col-lg-12">
                <h4>
                	<?php if ( !empty( $menu )) { ?>
                 		<a href ="<?php echo $menu['url']?>"><?php echo $menu['name']?></a>
                 		<?php } if (  !empty( $submenu )) {?>
                 		/ <a href ="<?php echo $submenu['url']?>"><?php echo $submenu['name']?></a>
                 		<?php } if (  !empty( $smenu )) {?>
                 		/ <a href ="<?php echo $smenu['url']?>"><?php echo $smenu['name']?></a>
                 		<?php } ?>
                 </h4>
                    <h1 class="page-header">Customer Complaints ( <?php echo (!empty( $complaints)?count($complaints):0)?> )</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php if ( !empty( $complaints ) ) { ?>
            <div id="paging_container8" class="paging_container8">
				<div class="page_navigation pull-right"></div>
            <?php 	foreach ( $complaints as $key => $values ) {   	?>
            	<div class="row">
					<div class="content">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Center Name : <?php echo $values['centername']?></b>
                        </div>
                        <div class="panel-body">
                        	<p>Complaints</p>
                            <p><?php echo $values['concern']?></p>
                            <div><span><b>Caller Details</b></span></div>
                            <table>
                        		<tbody>
                                    <tr>
                                       <td>Name</td>
                                       <td><b>&nbsp; : &nbsp;</b></td>
                                       <td><?php echo $values['caller_name']?></td>
                                    </tr>
                                     <tr>
                                       <td>Mobile</td>
                                       <td><b>&nbsp; : &nbsp;</b></td>
                                       <td><?php echo $values['mobile']?></td>
                                    </tr>
                                    <tr>
                                       <td>Address</td>
                                       <td><b>&nbsp; : &nbsp;</b></td>
                                       <td><?php echo $values['address']?></td>
                                    </tr>
                                    <tr>
                                       <td>Created</td>
                                       <td><b>&nbsp; : &nbsp;</b></td>
                                       <td><?php echo $values['created']?></td>
                                    </tr>
                                    <tr>
                                       <td>LastUpdated</td>
                                       <td><b>&nbsp; : &nbsp;</b></td>
                                       <td><?php echo $values['lastupdated']?></td>
                                    </tr>
                                </tbody>
                        	</table>
                        </div>
                        <div class="panel-footer">
								<b>CustomerCare Name : <?php echo $values['customercare']?></b>
						</div>
                    </div>
                </div>
                </div><!-- /.content -->
<!-- /.row -->
            </div>
            
            <?php }?>
            <div class="page_navigation pull-right"></div>
            </div>
            <?php } else{echo '<h2>Details Not Found</h2>';}?>
             </div>
