<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
	
	function __construct() {
		parent::__construct();
// 		$this->load->database();
	   	$this->load->helper(array('url','language'));
	   	$this->load->model('User_model','',TRUE);
	   	$this->load->helper('form');
		$this->load->library('session');
		
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
		header("Pragma: no-cache"); // HTTP 1.0.
		header("Expires: 0"); // Proxies.
		
		if ( !isset($this->session->userdata['s_roleguid']) || $this->session->userdata['s_roleguid']!=ADMIN_ROLE_ID ){
			redirect(base_url());
		}
	}
	
	public function index(){
		$sessionUserGuid 	= $this->session->userdata['s_userguid'];
		$partners	=	$this->User_model->getUserAndRoleDetails( '', PARTNER_ROLE_ID );
		$tellcaller	=	$this->User_model->getUserAndRoleDetails( '', EMPLOYEE_ROLE_ID );
		$customer	=	$this->User_model->customerDetails( );
		$complaints	=	$this->User_model->customerComplaintsDetails( );
		$this->data['partner'] = (!empty( $partners ) ? count($partners):0);
		$this->data['caller'] = (!empty( $tellcaller ) ? count($tellcaller):0);
		$this->data['customers'] = (!empty( $customer ) ? count($customer):0);
		$this->data['complaints'] = (!empty( $complaints ) ? count($complaints):0);
		$this->load->view('layout/header');
		$this->load->view('home/index',$this->data);
		$this->load->view('layout/footer');
	}
	
	public function complaints(){
		$sessionUserGuid 	= $this->session->userdata['s_userguid'];
		
		$this->data = array();
		$complaints = array();
		
		$date = '';
		$time = '';
		if (isset($_REQUEST['id'])&& !empty( $_REQUEST['id'] ) ) {
			$time = $_REQUEST['id'];
			$date = date(DATE_FORMAT,$_REQUEST['id']);
		}
		
		if ( isset( $_REQUEST['uid'] ) && !empty( $_REQUEST['uid']) ) {
			$complaints	=	$this->User_model->customerComplaintsDetails( '', '', $_REQUEST['uid'], $date );
			$this->data['menu']		= array('name' => 'CustomerCare', 'url' => base_url().'index.php/user/index/'.EMPLOYEE_ROLE_ID);
			$this->data['submenu']	= array('name' => 'Date Wise Complaints', 'url' => base_url().'index.php/home/complaintsdetails?uid='.$_REQUEST['uid']);
			$this->data['smenu']	= array('name' => 'Complaints', 'url' => base_url().'index.php/home/complaints?uid='.$_REQUEST['uid'].'&id='.$time);
		}elseif ( isset( $_REQUEST['pid'] ) && !empty( $_REQUEST['pid']) ){
			$complaints	=	$this->User_model->customerComplaintsDetails( $_REQUEST['pid'],'','',$date );
			$this->data['menu']		= array('name' => 'Partner', 'url' => base_url().'index.php/user/index/'.PARTNER_ROLE_ID);
			$this->data['submenu']	= array('name' => 'Date Wise Complaints', 'url' => base_url().'index.php/home/complaintsdetails?pid='.$_REQUEST['pid']);
			$this->data['smenu']	= array('name' => 'Complaints', 'url' => base_url().'index.php/home/complaints?pid='.$_REQUEST['pid'].'&id='.$time);
		}elseif($_REQUEST['rid'] == ADMIN_ROLE_ID){
			$complaints	=	$this->User_model->customerComplaintsDetails(  );
			$this->data['menu']	= array('name' => 'Complaints', 'url' => base_url().'index.php/home/complaints?rid='.ADMIN_ROLE_ID);
		}
		
		if ( !empty( $complaints ) ){
			$i=0;
			foreach ( $complaints as $key => $value ){
				$this->data['complaints'][$i]['caller_name']= (!empty( $value['caller_name'])?$value['caller_name']:'');
				$this->data['complaints'][$i]['mobile']		= (!empty( $value['mobile'])?$value['mobile']:'');
				$this->data['complaints'][$i]['address']	= (!empty( $value['address'])?$value['address']:'');
				$this->data['complaints'][$i]['concern']	= (!empty( $value['concern'])?$value['concern']:'');
				$this->data['complaints'][$i]['created']	= (!empty( $value['last_updated'])?$value['created']:'');
				$this->data['complaints'][$i]['lastupdated']= (!empty( $value['last_updated'])?$value['last_updated']:'');
				
				$partners	=	$this->User_model->getUserAndRoleDetails( $value['fitness_id'] );
				if ( !empty( $partners ) ) {
					$this->data['complaints'][$i]['centername']= (!empty( $partners[0]['center_name'])?$partners[0]['center_name']:'None');
				}
				
				$customercare	=	$this->User_model->getUserAndRoleDetails( $value['last_updated_by'] );
				if ( !empty( $customercare ) ) {
					$this->data['complaints'][$i]['customercare']= (!empty( $customercare[0]['username'])?$customercare[0]['username']:'None');
				}
				$i++;
			}
		}
		
		$this->load->view('layout/header');
		$this->load->view('home/complaints',$this->data);
		$this->load->view('layout/footer');
	}
	
	public function complaintsdetails(){
		$this->data['roleId'] 	= $this->session->userdata['s_roleguid'];
		$complaints = array();
		$userGuid	= '';
		$fitnessId	= '';
		if ( isset( $_REQUEST['uid'] ) && !empty( $_REQUEST['uid']) ) {
			$userGuid 	= $_REQUEST['uid'];
			$complaints	=	$this->User_model->customerComplaintsDetails( '', '', $_REQUEST['uid'], '','created' );
			$this->data['menu']		= array('name' => 'CustomerCare', 'url' => base_url().'index.php/user/index/'.EMPLOYEE_ROLE_ID);
			$this->data['submenu']	= array('name' => 'Date Wise Complaints', 'url' => base_url().'index.php/home/complaintsdetails?uid='.$_REQUEST['uid']);
			
			if ( !empty( $complaints ) ){
				$i=0;
				foreach ( $complaints as $key => $value ){
			
					$complaintsCount	=	$this->User_model->customerComplaintsDetails( '', '', $userGuid, $value['created']);
					if ( !empty( $complaintsCount ) ) {
						$this->data['complaints'][$i]['counts']= count($complaintsCount);
					}
			
					$this->data['complaints'][$i]['date']= $value['created'];
					$this->data['complaints'][$i]['userGuid']= $_REQUEST['uid'];
					$i++;
				}
			}
			$customercare	=	$this->User_model->getUserAndRoleDetails( $userGuid );
			if ( !empty( $customercare ) ) {
				$this->data['customercare_name']= (!empty( $customercare[0]['username'])?$customercare[0]['username']:'None');
			}
			$this->data['url']= base_url().'index.php/home/complaints?uid=';
			
		}elseif ( isset( $_REQUEST['pid'] ) && !empty( $_REQUEST['pid']) ){
			$fitnessId  = $_REQUEST['pid'];
			$complaints	=	$this->User_model->customerComplaintsDetails( $_REQUEST['pid'],'','','', 'created' );
			$this->data['menu']		= array('name' => 'Partner', 'url' => base_url().'index.php/user/index/'.PARTNER_ROLE_ID);
			$this->data['submenu']	= array('name' => 'Date Wise Complaints', 'url' => base_url().'index.php/home/complacomplaintsdetailsints?pid='.$_REQUEST['pid']);
			
			if ( !empty( $complaints ) ){
				$i=0;
				foreach ( $complaints as $key => $value ){
						
					$complaintsCount	=	$this->User_model->customerComplaintsDetails( $fitnessId, '', '', $value['created']);
					if ( !empty( $complaintsCount ) ) {
						$this->data['complaints'][$i]['counts']= count($complaintsCount);
					}
						
					$this->data['complaints'][$i]['date']= $value['created'];
					$this->data['complaints'][$i]['userGuid']= $_REQUEST['pid'];
					$i++;
				}
			}
			$customercare	=	$this->User_model->getUserAndRoleDetails( $fitnessId );
			if ( !empty( $customercare ) ) {
				$this->data['customercare_name']= (!empty( $customercare[0]['center_name'])?$customercare[0]['center_name']:'None');
			}
			$this->data['url']= base_url().'index.php/home/complaints?pid=';
			
		}
		
		$this->load->view('layout/header');
		$this->load->view('complaintList_view',$this->data);
		$this->load->view('layout/footer');
	}
	
	
}
