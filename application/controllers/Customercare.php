<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Customercare extends CI_Controller {
	
	function __construct() {
		parent::__construct();
// 		$this->load->database();
	   	$this->load->helper(array('url','language'));
	   	$this->load->model('User_model','',TRUE);
	   	$this->load->model('Customercare_model','',TRUE);
	   	$this->load->model('Mail','',TRUE);
	   	$this->load->helper('form');
	   	$this->load->model('Utils','',TRUE);
		$this->load->library('session');
		
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
		header("Pragma: no-cache"); // HTTP 1.0.
		header("Expires: 0"); // Proxies.
		
		if ( !isset($this->session->userdata['s_roleguid']) || $this->session->userdata['s_roleguid']!=EMPLOYEE_ROLE_ID ){
			redirect(base_url());
		}
	}
	
	public function index(){
		$sessionUserGuid 			= $this->session->userdata['s_userguid'];
		$this->data['userguid'] = $sessionUserGuid;
		$complaints	=	$this->User_model->customerComplaintsDetails( '', '', $sessionUserGuid);
		$this->data['complaint'] = (!empty( $complaints )?count($complaints):0);
		$this->load->view('layout1/header');
		$this->load->view('customercare/index',$this->data);
		$this->load->view('layout1/footer');
	}
	
	public function market(){
		$sessionUserGuid 	= $this->session->userdata['s_userguid'];
		
		if(empty($_REQUEST['cid'])){
 			redirect(base_url().'index.php/customercare');
 		}
 		
 		$id = $_REQUEST['cid'];
 		/** variable creation  */
 		$this->data['menu']	= array();
 			
 		switch ($id) {
 
 			case MARKET_FITNESS_ID:{
 				$this->data['menu']		= $this->Customercare_model->subMenuDetails( MARKET_FITNESS_ID );
 				$this->data['url']		= base_url().'index.php/customercare/market?cid='.MARKET_FITNESS_ID;
 				$this->data['title']	= MARKET_TYPE_1;
 				$this->data['icon']		= 'fa fa-users fa-5x';
 				$this->data['color']	= 'panel-primary';
 				break;
 			}
 			case MARKET_NUTRITION_ID:{
 				$this->data['menu']		= $this->Customercare_model->subMenuDetails( MARKET_NUTRITION_ID );
 				$this->data['url']		= base_url().'index.php/customercare/market?cid='.MARKET_NUTRITION_ID;
 				$this->data['title']	= MARKET_TYPE_2;
 				$this->data['icon']		= 'fa fa-fire fa-5x';
 				$this->data['color']	= 'panel-green';
 				break;
 			}
 			
 			case MARKET_ORGANIC_ID:{
 				$this->data['menu']		= $this->Customercare_model->subMenuDetails( MARKET_ORGANIC_ID );
 				$this->data['url']		= base_url().'index.php/customercare/market?cid='.MARKET_ORGANIC_ID;
 				$this->data['title']	= MARKET_TYPE_3;
 				$this->data['icon']		= 'fa fa-tree fa-5x';
 				$this->data['color']	= 'panel-yellow';
 				break;
 			}
 			case MARKET_PERSONAL_ID:{
 				$this->data['menu']		= $this->Customercare_model->subMenuDetails( MARKET_PERSONAL_ID );
 				$this->data['url']		= base_url().'index.php/customercare/market?cid='.MARKET_PERSONAL_ID;
 				$this->data['title']	= MARKET_TYPE_4;
 				$this->data['icon']		= 'fa fa-stethoscope fa-5x';
 				$this->data['color']	= 'panel-red';
 				break;
 			}
 			default:{
 				redirect(base_url().'index.php/customercare');
 				break;
 			}
 		}
//  		echo '<pre>';print_r($this->data);exit;
		$this->load->view('layout1/header');
		$this->load->view('customercare/menu', $this->data);
		$this->load->view('layout1/footer');
	}
	
	public function search( $id, $roleguid='' ){
		if ( empty( $id ) ){
			redirect(base_url().'index.php/customercare');
		}
		
		$this->data['subMenuId'] = $id;
		$menu = $this->Customercare_model->subMenuDetails( '', $id );
		if ( !empty( $menu )){
			$menuId 	= (!empty( $menu[0]['menu_id'] ) ?$menu[0]['menu_id'] :'');
			$roleId 	= (!empty( $menu[0]['role_id'] ) ?$menu[0]['role_id'] :'');
			$menuName 	= (!empty( $menu[0]['name'] ) ?$menu[0]['name'] :'');
			$this->data['menu']		= array('name' => constant( "MARKET_TYPE_".$menuId), 'url' => base_url().'index.php/customercare/market?cid='.$menuId); 
			$this->data['submenu']	= array('name' => $menuName, 'url' => base_url().'index.php/customercare/search/'.$id.'/'.$roleId);
		}
		$submenu ='';
		if ( !empty( $roleguid)) {
			$submenu = $this->Customercare_model->subMenuDetails( '', $id, $roleguid );
		}
		
		if ( !empty( $submenu )) {
			$partner = $this->User_model->getUserAndRoleDetails('', $roleguid);
			if ( !empty( $partner )){
				foreach ( $partner as $key=>$value ){
					$fitnessId[] = $value['userGuid'];
				}
				if ( isset( $this->session->userdata['fitnessId'] ) ) {
					$fitnessId = $this->session->userdata['fitnessId'];
				}
				if (!empty( $fitnessId )) {
// 					$fitnessList= $this->User_model->customerFitnessCenterDetails('', $fitnessId);
					$i=0;
// 					$userList = array();
// 					$data	=	array();
					$data1	=	array();
					/*foreach ( $fitnessList as $key1=>$value1 ){
						$userList[]	  = $value1['fitness_id'];
						 $customerList = $this->User_model->customerDetails( '',  $value1['customer_id']);
						if ( !empty( $customerList )) {
							$data[$i]['customername'] = (!empty( $customerList[0]['name'])?$customerList[0]['name']:'');
							$data[$i]['customerId'] = (!empty( $customerList[0]['userGuid'])?$customerList[0]['userGuid']:'');
						} 
						
						$partnerList = $this->User_model->getUserAndRoleDetails($value1['fitness_id'], PARTNER_ROLE_ID);
						if ( !empty( $partnerList )) {
							$data[$i]['centerId'] 		= (!empty( $partnerList[0]['userGuid'])?$partnerList[0]['userGuid']:'');
							$data[$i]['center_name'] 	= (!empty( $partnerList[0]['center_name'])?$partnerList[0]['center_name']:'');
							$data[$i]['username'] 		= (!empty( $partnerList[0]['username'])?$partnerList[0]['username']:'');
							$data[$i]['web_url'] 		= (!empty( $partnerList[0]['web_url'])?$partnerList[0]['web_url']:'');
						}
						$i++;
					}*/
					
					$finessData = $this->User_model->getUserAndRoleDetails($fitnessId, PARTNER_ROLE_ID);
						if ( !empty( $finessData )) {
							$j=0;
							foreach ( $finessData as $key2=>$value2 ){
								$data1[$j]['centerId'] 		= (!empty( $value2['userGuid'])?$value2['userGuid']:'');
								$data1[$j]['center_name'] 	= (!empty( $value2['center_name'])?$value2['center_name']:'');
								$data1[$j]['username'] 		= (!empty( $value2['username'])?$value2['username']:'');
								$data1[$j]['web_url'] 		= (!empty( $value2['web_url'])?$value2['web_url']:'');
								$j++;
							}
						}
					}
// 				$this->data['fitness'] = array_merge($data, $data1);
				$this->data['fitness'] = $data1;
			}
		}
		$this->load->view('layout1/header');
		$this->load->view('customercare/search', $this->data);
		$this->load->view('layout1/footer');
	}
	
	public function complaints(){
		if ( empty( $_REQUEST['cid']) || empty( $_REQUEST['sid'])){
			redirect(base_url());
		}
		
		$message = $this->session->flashdata('message');
		if ( $message){
			$this->data['message'] = $message;
		}
		
		$this->data['cid']		= $_REQUEST['cid'];
		$this->data['sid']		= $_REQUEST['sid'];
		$menu = $this->Customercare_model->subMenuDetails( '', $_REQUEST['sid'] );
		if ( !empty( $menu )){
			$menuId 	= (!empty( $menu[0]['menu_id'] ) ?$menu[0]['menu_id'] :'');
			$roleId 	= (!empty( $menu[0]['role_id'] ) ?$menu[0]['role_id'] :'');
			$menuName 	= (!empty( $menu[0]['name'] ) ?$menu[0]['name'] :'');
			$this->data['menu']		= array('name' => constant( "MARKET_TYPE_".$menuId), 'url' => base_url().'index.php/customercare/market?cid='.$menuId);
			$this->data['submenu']	= array('name' => $menuName, 'url' => base_url().'index.php/customercare/search/'.$_REQUEST['sid'].'/'.$roleId);
		}
		
		
		$this->data['centerId'] = $_REQUEST['cid'];
		$finessId	=	'';
		if ( isset( $this->session->userdata['fitnessId'] ) ) {
			$finessId = $this->session->userdata['fitnessId'];
		}
		
		$fitnessDetails = $this->User_model->getUserAndRoleDetails($finessId, PARTNER_ROLE_ID);
		$this->data['centerNameList']= array(''=>'---select centername ---');
		if ( !empty( $fitnessDetails )) {
			foreach ( $fitnessDetails as $fitness){
		
				if ( !empty( $fitness['center_name'] )){
					$this->data['centerNameList'][$fitness['userGuid']]=$fitness['center_name'];
				}
			}
		}
		$this->data['user']	=	$this->User_model->getUserAndRoleDetails( $_REQUEST['cid'] );
		
		if ( !empty( $_REQUEST['uid'])){
			$customer	=	$this->User_model->customerDetails( '', $_REQUEST['uid'] );
			if ( !empty( $customer[0])){
				$this->data['customer'] = $customer[0];
			}
		}
		
		$this->load->view('layout1/header');
		$this->load->view('customercare/complaints',$this->data); 
		$this->load->view('layout1/footer');
	}
	function getcenternameprfile( $cid ) {
		$this->data['data']= 'Details not fount';
		if ( empty( $cid ) ){
			echo json_encode($this->data);
		}
		$html = '';
		$centerInfo	=	$this->User_model->getUserAndRoleDetails( $cid );
		if ( !empty( $centerInfo)) {
			foreach ( $centerInfo as $key=>$value ) {
				$image	=	(!empty($value['photo'])? base_url().'assets/upload_images/'.$value['photo']:base_url().'assets/upload_images/'.'default.jpeg');
				$photo	=	(!empty($value['photo'])? $value['photo']:'');
				$uname	=	(!empty( $value['username'] ) ? $value['username'] : '');
				$email	=	(!empty( $value['email'] ) ? $value['email'] : '');
				$phone	=	(!empty( $value['phone'] ) ? $value['phone'] : '');
				$userGuid	=	(!empty( $value['userGuid'] ) ? $value['userGuid'] : '');
				$centername	=	(!empty( $value['center_name'] ) ? $value['center_name'] : '');
				$weburl		=	(!empty( $value['web_url'] ) ? $value['web_url'] : '');
				$packages	=	(!empty( $value['packages'] ) ? $value['packages'] : '');
				$offers		=	(!empty( $value['offers'] ) ? $value['offers'] : '');
				$valueAdded	=	(!empty( $value['value_added_service'] ) ? $value['value_added_service'] : '');
				$gender		=	(!empty( $value['gender'] ) ? $value['gender'] : '');
				$address	=	(!empty( $value['address'] ) ? $value['address'] : '');
			}
			$html = '<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="'.$image.'" class="img-responsive" height="150" width="150"> </div>
				          <div class="col-lg-6"> 
                            <table class="table user">  
				            <tr>
				              <th>Center Name</th><td>'. $centername.'</td>
				            </tr>
				            <tr>
				              <th>User Name</th><td>'. $uname.'</td>
				            </tr>
				            <tr>
				              <th>Email</th><td>'. $email.'</td>
				            </tr>
				            <tr>
				              <th>Mobile  </th><td>'. $phone.'</td>
				            </tr>
				            <tr>
				              <th>Gender  </th><td>'. $gender.'</td>
				            </tr>
				            <tr>
				              <th>Address  </th><td>'. $address.'</td>
				            </tr>
				            <tr>
				              <th>Web URL  </th><td>'. $weburl.'</td>
				            </tr>
				            <tr>
				              <th>Pakages  </th><td>'. $packages.'</td>
				            </tr>
				            <tr>
				              <th>Offers  </th><td>'. $offers.'</td>
				            </tr>
				            <tr>
				              <th>Value Added  </th><td>'. $valueAdded.'</td>
				            </tr>
				          </table>
			          </div>';
			
			$this->data['data']= $html;
		}
		echo json_encode($this->data);exit;
	}
	
	function addcomplaints(){
		if ( empty( $_REQUEST['cid']) || empty( $_REQUEST['sid'])){
			redirect(base_url());
		}
		$sessionUserGuid 	= $this->session->userdata['s_userguid'];
		
		$message = 'Failure to insert please go back and come';
		if (!empty( $_REQUEST['caller_name']) && !empty( $_REQUEST['mobile']) && !empty( $_REQUEST['concern']) && !empty( $_REQUEST['centername'])){
			$updated		=	date(DATE_TIME_FORMAT);
			$created		=	date(DATE_FORMAT);
			$refrence_no	=	uniqid();
			$guid			=	$this->Utils->getGuid();
			$insertData = array(	'caller_name'		=> $_REQUEST['caller_name'],
									'mobile'			=> $_REQUEST['mobile'],
									'address'			=> (!empty( $_REQUEST['address'] )?$_REQUEST['address']:''),
									'concern'			=> $_REQUEST['concern'],
									'fitness_id'		=> $_REQUEST['centername'],
									'customer_id'		=> (!empty( $_REQUEST['customerId'] )?$_REQUEST['customerId']:''),
									'refrence_no'		=> $refrence_no,
									'created'			=> $created,
									'created_by'		=> $sessionUserGuid,
									'last_updated'		=> $updated,
									'last_updated_by'	=> $sessionUserGuid,
									'guid'				=> $guid
								);
			$insert	=	$this->User_model->insertCustomerComplaints( $insertData );
			if ( $insert ){
				$exists	=	 $this->User_model->userDetails( '', $_REQUEST['centername'] );
				
				if ( $exists[0]['email'] ) {
					$_REQUEST['refrence_no'] =$refrence_no;
					$subject	=	'Complaint Registered';
					$body 		= 	$this->load->view('template/email.php',$_REQUEST,TRUE);
					$send		=	 $this->Mail->sendmail( $to ='',$exists[0]['email'], $subject, $body ,$attachment ='' );
				}
				$message = 'Insert successfully';
			}
			
			$this->session->set_flashdata('message', $message);
			redirect(base_url().'index.php/customercare/message?id='.$guid);
		}
		$this->session->set_flashdata('message', $message);
		redirect(base_url().'index.php/customercare/complaints?cid='.$_REQUEST['cid'].'&sid='.$_REQUEST['sid']);
	}
	
	public function complaintlist(){
		$sessionUserGuid 			= $this->session->userdata['s_userguid'];
		$this->data['userguid'] 	= $sessionUserGuid;
		$date = '';
		$this->data['menu']		= array('name' => 'Complaints', 'url' => base_url().'index.php/customercare/complaintlist');
		if (isset($_REQUEST['id'])&& !empty( $_REQUEST['id'] ) ) {
			$date = date(DATE_FORMAT,$_REQUEST['id']);
			$this->data['menu']			= array('name' => 'Date Wise Complaints', 'url' => base_url().'index.php/customercare/complaintsdetails');
			$this->data['submenu']		= array('name' => 'Complaints', 'url' => base_url().'index.php/customercare/complaintlist?id='.$_REQUEST['id']);
		}
		
		$complaints	=	$this->User_model->customerComplaintsDetails( '', '', $sessionUserGuid, $date);
		if ( !empty( $complaints ) ){
			$i=0;
			foreach ( $complaints as $key => $value ){
				$this->data['complaints'][$i]['caller_name']= (!empty( $value['caller_name'])?$value['caller_name']:'');
				$this->data['complaints'][$i]['mobile']		= (!empty( $value['mobile'])?$value['mobile']:'');
				$this->data['complaints'][$i]['address']	= (!empty( $value['address'])?$value['address']:'');
				$this->data['complaints'][$i]['concern']	= (!empty( $value['concern'])?$value['concern']:'');
				$this->data['complaints'][$i]['created']	= (!empty( $value['last_updated'])?$value['created']:'');
				$this->data['complaints'][$i]['lastupdated']= (!empty( $value['last_updated'])?$value['last_updated']:'');
	
				$partners	=	$this->User_model->getUserAndRoleDetails( $value['fitness_id'] );
				if ( !empty( $partners ) ) {
					$this->data['complaints'][$i]['centername']= (!empty( $partners[0]['center_name'])?$partners[0]['center_name']:'None');
				}
	
				$customercare	=	$this->User_model->getUserAndRoleDetails( $value['last_updated_by'] );
				if ( !empty( $customercare ) ) {
					$this->data['complaints'][$i]['customercare']= (!empty( $customercare[0]['username'])?$customercare[0]['username']:'None');
				}
				$i++;
			}
		}
		
		$this->load->view('layout1/header');
		$this->load->view('home/complaints',$this->data);
		$this->load->view('layout1/footer');
	}
	
	public function message(){
		$this->data = '';
		if ( !empty( $_REQUEST['id'])) {
			$complaints	=	$this->User_model->customerComplaintsDetails( '', $_REQUEST['id'] );
			
			if ( !empty( $complaints )) {
					$this->data['name'] 		= (!empty( $complaints[0]['caller_name'])?$complaints[0]['caller_name']:'');
					$this->data['mobile'] 		= (!empty( $complaints[0]['mobile'])?$complaints[0]['mobile']:'');
					$this->data['refrence_no'] 	= (!empty( $complaints[0]['refrence_no'])?$complaints[0]['refrence_no']:'');
			}
		}
		$this->load->view('customercare/success',$this->data);
		//echo '<pre>';print_r($this->data);exit;
	}
	public function complaintsdetails(){
		$sessionUserGuid 		= $this->session->userdata['s_userguid'];
		$this->data['userguid'] = $sessionUserGuid;
		$this->data['roleId'] 	= $this->session->userdata['s_roleguid'];
		$complaints	=	$this->User_model->customerComplaintsDetails( '', '', $sessionUserGuid,'', 'created');
		if ( !empty( $complaints ) ){
			$i=0;
			foreach ( $complaints as $key => $value ){
		
				$complaintsCount	=	$this->User_model->customerComplaintsDetails( '', '', $sessionUserGuid, $value['created']);
				if ( !empty( $complaintsCount ) ) {
					$this->data['complaints'][$i]['counts']= count($complaintsCount);
				}
				
				$this->data['complaints'][$i]['date']= $value['created'];
				$this->data['complaints'][$i]['userGuid']= $value['last_updated_by'];
				$i++;
			}
		}
		$customercare	=	$this->User_model->getUserAndRoleDetails( $sessionUserGuid );
		if ( !empty( $customercare ) ) {
			$this->data['customercare_name']= (!empty( $customercare[0]['username'])?$customercare[0]['username']:'None');
		}
		$this->data['menu']	= array('name' => 'Date Wise Complaints', 'url' => base_url().'index.php/customercare/complaintsdetails');
		$this->data['url']= base_url().'index.php/customercare/complaintlist?uid=';
		$this->load->view('layout1/header');
		$this->load->view('complaintList_view',$this->data);
		$this->load->view('layout1/footer');
	}
}
