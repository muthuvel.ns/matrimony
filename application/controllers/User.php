<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	   $this->load->helper(array('url','language'));
	   $this->load->helper('form');
		$this->load->model('User_model','',TRUE);
		$this->load->model('Utils','',TRUE);
		$this->load->library('session');
		
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
		header("Pragma: no-cache"); // HTTP 1.0.
		header("Expires: 0"); // Proxies.
		if ( !isset($this->session->userdata['s_roleguid']) || $this->session->userdata['s_roleguid'] != ADMIN_ROLE_ID ){
			 redirect(base_url());
		}
	}
	
	public function index( $roleguid ){
		if ( empty( $roleguid )){
			redirect(base_url());
		}
		$this->data['userlist'] = $this->User_model->getUserAndRoleDetails('', $roleguid);
// 		echo '<pre>';print_r($this->data);exit;
		$this->load->view('layout/header');
		if ( $roleguid == PARTNER_ROLE_ID ) {
			$this->load->view('user/partner', $this->data);
		}elseif ( $roleguid == EMPLOYEE_ROLE_ID ){
			$this->load->view('user/employee', $this->data);
		}else{
			redirect(base_url());
		}
		$this->load->view('layout/footer');
	}
	
	public function customerlist( ){
		$this->data['userlist'] = $this->User_model->customerDetails();
		// 		echo '<pre>';print_r($this->data);exit;
		$this->load->view('layout/header');
		$this->load->view('user/customer', $this->data);
		$this->load->view('layout/footer');
	}
	
	public function profile( $userguid , $roleguid){
		if ( empty( $roleguid ) || empty( $userguid )){
			redirect(base_url());
		}
		
		$this->data	=	array();
		$message = $this->session->flashdata('message');
		if ( $message){
			$this->data['message'] = $message;
		}
		if ( $roleguid == EMPLOYEE_ROLE_ID ){
			$this->data['menu']		= array('name' => "CustomerCare", 'url' => base_url().'index.php/user/index/'.$roleguid);
			$this->data['submenu']	= array('name' => "Profile", 'url' => base_url().'index.php/user/profile/'.$userguid.'/'.$roleguid);
		}elseif ( $roleguid == PARTNER_ROLE_ID ){
			$this->data['menu']		= array('name' => "Fitness center", 'url' => base_url().'index.php/user/index/'.$roleguid);
			$this->data['submenu']	= array('name' => "Profile", 'url' => base_url().'index.php/user/profile/'.$userguid.'/'.$roleguid);
		}
		$this->data['userguid']= $userguid;
		$this->data['roleguid']= $roleguid;
		$this->data['user']	=	$this->User_model->getUserAndRoleDetails( $userguid );
		$this->load->view('layout/header');
		$this->load->view('user/profile_view.php',$this->data);
		$this->load->view('layout/footer');
	}
	
	public function customerprofile( $userguid ){
		if ( empty( $userguid )){
			redirect(base_url());
		}
	
		$this->data	=	array();
		$message = $this->session->flashdata('message');
		if ( $message){
			$this->data['message'] = $message;
		}
	
		$this->data['userguid']= $userguid;
		$this->data['user']	=	$this->User_model->customerDetails( '', $userguid );
		$this->load->view('layout/header');
		$this->load->view('user/userprofile.php',$this->data);
		$this->load->view('layout/footer');
	}
	
	public function editprofile( $userGuid, $roleguid ){
		if ( empty( $roleguid ) || empty( $userGuid )){
			redirect(base_url());
		}
		
		$sessionUserGuid =  $this->session->userdata['s_userguid'];
		$updateUserData	=	array();
		$updateProfileData	=	array();
		if ( !empty( $_REQUEST['username'] ) && isset( $_REQUEST['username'] )) {
			$updateUserData['username'] = $_REQUEST['username'];
		}
		if ( !empty( $_REQUEST['email'] ) && isset( $_REQUEST['email'] ) ) {
			$updateUserData['email'] = $_REQUEST['email'];
		}
		if ( !empty( $_REQUEST['pass'] ) && isset( $_REQUEST['pass'] ) && !empty( $_REQUEST['pass_confirmation'] ) && isset( $_REQUEST['pass_confirmation'] )) {
			$updateUserData['password'] = md5($_REQUEST['pass']);
		}
		if ( !empty( $_REQUEST['phone'] ) && isset( $_REQUEST['phone'] ) ) {
			$updateProfileData['phone'] = $_REQUEST['phone'];
		}
			
		$updateProfileData['gender'] 				= ( !empty( $_REQUEST['gender'] )?$_REQUEST['gender']:'other');
		$updateProfileData['address'] 				= ( !empty( $_REQUEST['address'] )?$_REQUEST['address']:'');
		
		if ( $roleguid == PARTNER_ROLE_ID ) {
			if ( !empty( $_REQUEST['web_url'] ) && isset( $_REQUEST['web_url'] ) ) {
				$updateProfileData['web_url'] = $_REQUEST['web_url'];
			}
			if ( !empty( $_REQUEST['centername'] ) && isset( $_REQUEST['centername'] ) ) {
				$updateProfileData['center_name'] = $_REQUEST['centername'];
			}
				
			$updateProfileData['packages']				= ( !empty( $_REQUEST['package'] )?$_REQUEST['package']:'');
			$updateProfileData['offers']				= ( !empty( $_REQUEST['offer'] )?$_REQUEST['offer']:'');
			$updateProfileData['value_added_service']	= ( !empty( $_REQUEST['value_added_service'] )?$_REQUEST['value_added_service']:'');
		}
		
		if ( isset( $_FILES['photo']['error'] ) && $_FILES['photo']['error'] == 0) {
			$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/";
			$config['allowed_types'] = '*';
			// 	 			$config['max_size'] = '100';
			// 	 			$config['max_width']  = '1024';
			// 	 			$config['max_height']  = '768';
			$config['file_name']  = $userGuid.'_'.strtotime(date(DATE_TIME_FORMAT));
			
			$this->load->library('upload', $config);
		 	
			if ( ! $this->upload->do_upload('photo')) {
				$message = $this->upload->display_errors();
			}else{
				$data	= array('upload_data' => $this->upload->data());
				$updateProfileData['photo'] =(!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:'');
			}
		}
		
		$this->db->trans_begin();
		$created 									=   date(DATE_TIME_FORMAT);
		$updateUserData['last_updated']     		=  $created;
		$updateUserData['last_updated_by']      	=  $sessionUserGuid;
		if ( !empty( $updateUserData ) ) {
			$insertUser	    =   $this->User_model->updateUser($userGuid, $updateUserData);
		}
	
		$updateProfileData['last_updated']     		=  $created;
		$updateProfileData['last_updated_by']      	=  $sessionUserGuid;
		if ( !empty( $updateProfileData ) ) {
			$insertProfile        =   $this->User_model->updateUserProfile($userGuid, $updateProfileData);
		}
			
		if (empty( $insertUser ) || empty( $insertProfile ) )   {
			$this->db->trans_rollback();
			$message = "Update Failed";
		}else {
			$message = "Update Success";
			$this->db->trans_commit();
		}
		
		$this->session->set_flashdata('message', $message);
		redirect('index.php/user/profile/'.$userGuid.'/'.$roleguid);
	}
	
	
	public function editcustomerprofile( $userGuid ){
		if ( empty( $userGuid )){
			redirect(base_url());
		}
	
		$sessionUserGuid =  $this->session->userdata['s_userguid'];
		$updateUserData	=	array();
		if ( !empty( $_REQUEST['username'] ) && isset( $_REQUEST['username'] )) {
			$updateUserData['name'] = $_REQUEST['username'];
		}
		if ( !empty( $_REQUEST['email'] ) && isset( $_REQUEST['email'] ) ) {
			$updateUserData['email'] = $_REQUEST['email'];
		}
		if ( !empty( $_REQUEST['pass'] ) && isset( $_REQUEST['pass'] ) && !empty( $_REQUEST['pass_confirmation'] ) && isset( $_REQUEST['pass_confirmation'] )) {
			$updateUserData['password'] = md5($_REQUEST['pass']);
		}
		if ( !empty( $_REQUEST['phone'] ) && isset( $_REQUEST['phone'] ) ) {
			$updateUserData['mobile'] = $_REQUEST['phone'];
		}
			
		$updateUserData['gender'] 				= ( !empty( $_REQUEST['gender'] )?$_REQUEST['gender']:'other');
		$updateUserData['address'] 				= ( !empty( $_REQUEST['address'] )?$_REQUEST['address']:'');
	
		if ( isset( $_FILES['photo']['error'] ) && $_FILES['photo']['error'] == 0) {
			$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/";
			$config['allowed_types'] = '*';
			// 	 			$config['max_size'] = '100';
			// 	 			$config['max_width']  = '1024';
			// 	 			$config['max_height']  = '768';
			$config['file_name']  = $userGuid.'_'.strtotime(date(DATE_TIME_FORMAT));
				
			$this->load->library('upload', $config);
	
			if ( ! $this->upload->do_upload('photo')) {
				$message = $this->upload->display_errors();
			}else{
				$data	= array('upload_data' => $this->upload->data());
				$updateUserData['photo'] =(!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:'');
			}
		}
	
		$created 									=   date(DATE_TIME_FORMAT);
		$updateUserData['last_updated']     		=  $created;
		$updateUserData['last_updated_by']      	=  $sessionUserGuid;
		if ( !empty( $updateUserData ) ) {
			$insertUser	    =   $this->User_model->updateCustomerInfo($userGuid, $updateUserData);
		}
	
		$message = "Update Success";
		if (empty( $insertUser ) )   {
			$message = "Update Failed";
		}
	
		$this->session->set_flashdata('message', $message);
		redirect('index.php/user/customerprofile/'.$userGuid);
	}
	
	function disable(){
	
		$message 		= '';
		$url			= 0;
		$result 		= 0;
		$table			=	array();
		$sessionUserGuid =  $this->session->userdata['s_userguid'];
		$created		 =	date(DATE_TIME_FORMAT);
		$userTrackingDetails = array();
	
		if ( isset( $_REQUEST['uid'] ) && !empty( $_REQUEST['uid'] )  && isset( $_REQUEST['rid'] ) && !empty( $_REQUEST['rid'] )) {
			$updateData['deleted']			 =	 1;
			$updateData['last_updated']      =  $created;
			$updateData['last_updated_by']   =  $sessionUserGuid;
			
			$exits	=	$this->User_model->getUserAndRoleDetails( $_REQUEST['uid'] );
			$message = 'user not fount';
			if ( $exits ) {
				$result	    =   $this->User_model->updateUser($_REQUEST['uid'], $updateData);
				$result	    =   $this->User_model->updateUserProfile($_REQUEST['uid'], $updateData);
				$result	    =   $this->User_model->updateUserRole($_REQUEST['uid'], $updateData);
				
				$message = 'Update Failure';
				if ( !empty( $result ) ) {
					$message = 'Update Successfully';
				}
			}
			$url 		=	base_url().'index.php/user/index/'.$_REQUEST['rid'];
		
		}elseif ( isset( $_REQUEST['uid'] ) && !empty( $_REQUEST['uid'] ) && !isset( $_REQUEST['rid'] ) && empty( $_REQUEST['rid'] )){
			$updateData['deleted']			 =	 1;
			$updateData['last_updated']      =  $created;
			$updateData['last_updated_by']   =  $sessionUserGuid;
			$exits		=	$this->User_model->customerDetails( '', $_REQUEST['uid'] );
			$message	= 'user not fount';
			if ( $exits ) {
				$result	    =   $this->User_model->updateCustomerInfo($_REQUEST['uid'], $updateData);
				
				$message = 'Update Failure';
				if ( !empty( $result ) ) {
					$message = 'Update Successfully';
				}
			}
			$url 		=	base_url().'index.php/user/customerlist';
		}
	
		$this->session->set_flashdata('message', $message);
		if ( $url ) {
			redirect($url);
		}
			
		redirect(base_url());
	}
	
}