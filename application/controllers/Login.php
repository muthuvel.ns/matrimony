<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->helper(array('url','language'));
   $this->load->model('User_model','',TRUE);
   $this->load->model('Login_model','',TRUE);
   $this->load->library('session');
   
   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
   header("Pragma: no-cache"); // HTTP 1.0.
   header("Expires: 0"); // Proxies.
   
    if ( isset($this->session->userdata['s_roleguid']) && !empty( $this->session->userdata['s_roleguid']) ){
    	/** Get redirection path by passing user roleguid  */
 		$loginPath		= $this->Login_model->loginRedirection($this->session->userdata['s_roleguid']);//echo '<pre>';print_r($loginPath);exit;
 		redirect($loginPath);
   } 
 }
 
 function index() {
 	$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
 	$this->output->set_header("Pragma: no-cache");
 	
 	$this->data['message']	=	'';
 	if( $this->input->post() ) {
 		if ( !empty( $_REQUEST['username'] ) && !empty( $_REQUEST['password'] )) {
	 		$_REQUEST['username']	= strtolower($_REQUEST['username']);
	 		if($this->isValidLoginForm($_REQUEST)) {
	 			$formvalues	=	$this->input->post();
	 			$userdata	=	$this->Login_model->loginValidation( $formvalues );
	 			if ( !empty( $userdata ) )	{
		 			/** check email count is one then redirect to crossponding user domain*/
		 			if( count($userdata) == 1 ) {/** User has one role */
		 				/** Get user name  */
		 				$userProfileName	= ucfirst($userdata['0']['username']);
		 				$userProfile		= $this->User_model->userProfileDetails( $userdata['0']['userGuid'] );
		 				$imagePath			= (!empty( $userProfile['0']['photo'] ) ? $userProfile['0']['photo']:'');
		 				
		 				$privateLogin = $this->Login_model->getPrivateLoginInfo( $userdata['0']['userGuid'] );
		 				
		 				if ( !empty( $privateLogin['0'] )) {
		 					$data['fitnessId']				= $privateLogin['0']['fitness_id'];
		 				}
		 	
		 				/** Set session value  */ 
		 				$data['s_email']			= $userdata['0']['email'];
		 				$data['s_roleName']			= $userdata['0']['roleName'];
		 				$data['s_userguid']			= $userdata['0']['userGuid'];
		 				$data['s_roleguid']			= $userdata['0']['userRoleGuid'];
		 				$data['s_username']			= $userProfileName;
		 				$data['s_imagepath']		= $imagePath;
		 	
		 				$this->session->set_userdata($data);
		 	
		 				/** Login activity  */
		 				$loginActivity	= $this->Login_model->loginActivityEntry($userdata['0']['userGuid'], 1, 'Cloud');
		 				
		 				/** Get redirection path by passing user roleguid  */
		 				$loginPath		= $this->Login_model->loginRedirection($userdata['0']['userRoleGuid']);//echo '<pre>';print_r($loginPath);exit;
		 				redirect($loginPath);
		 				$this->data['message']	=	'Success';
		 				$this->data['status']	=	200;
		 			}
	 			}else{
	 				$this->data['message']	=	'Username or Password you entered is incorrect';
	 				$this->data['status']	=	404;
	 			}
	 		}else{
	 			$this->data['message']	=	'Invalid Email Address';
	 			$this->data['status']	=	404;
	 		}
 		}else{
	 			$this->data['message']	=	'Username or Password shoud not empty';
	 			$this->data['status']	=	404;
	 		}
 	}
//  	echo json_encode($this->data);exit;
   $this->load->view('login/index', $this->data);
 }
 
 
 function isValidLoginForm( $request ) {
 	/** Check email structure validation  */
 	return preg_match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$^', $request['username']);
 }
 
 
}
 
?>