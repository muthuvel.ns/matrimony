<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Registration extends CI_Controller {
 
 function __construct() {
   parent::__construct();
   $this->load->helper(array('url','language'));
   $this->load->helper('form');
   $this->load->model('User_model','',TRUE);
   $this->load->model('Registration_model','',TRUE);
   $this->load->model('Utils','',TRUE);
   $this->load->library('session');
  
   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
   header("Pragma: no-cache"); // HTTP 1.0.
   header("Expires: 0"); // Proxies.
 	if ( !isset($this->session->userdata['s_roleguid']) || $this->session->userdata['s_roleguid']!=ADMIN_ROLE_ID ){
			redirect(base_url());
	}
   
 }
 
 function index(  ){
 	if ( empty($_REQUEST['rid']) ){
 		redirect(base_url());
 	}
 	
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$this->data['message'] = $message;
 	}
 	
 	$this->data['roleGuid'] = $_REQUEST['rid'];
// 	echo json_encode($this->data);exit;
	$this->load->view('layout/header');
	$this->load->view('registration/index', $this->data);
	$this->load->view('layout/footer');
   
 }
 
 function customercare(  ){
 	if ( empty($_REQUEST['rid']) || empty($_REQUEST['fid'])){
 		redirect(base_url());
 	}
 	
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$this->data['message'] = $message;
 	}
 	/** get fitness center details by given fitness id */
 	$fitnessDetails = $this->User_model->getUserAndRoleDetails('', $_REQUEST['fid']);
 	$this->data['centerNameList']= array(''=>'---select centername ---');
 	if ( !empty( $fitnessDetails )) {
 		foreach ( $fitnessDetails as $fitness){
 			if ( !empty( $fitness['center_name'] )){
 				$this->data['centerNameList'][$fitness['userGuid']]=$fitness['center_name'];
 			}
 		}
 	}
 	$this->data['roleGuid'] = $_REQUEST['rid'];
// echo '<pre>';print_r($this->data);exit;
 	$this->load->view('layout/header');
 	$this->load->view('registration/employee', $this->data);
 	$this->load->view('layout/footer');
 	 
 }
 
 function user(  ){
 	
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$this->data['message'] = $message;
 	}
 	
 	$fitnessDetails = $this->User_model->getUserAndRoleDetails('', PARTNER_ROLE_ID);
 	$this->data['centerNameList']= array(''=>'---select centername ---');
 	if ( !empty( $fitnessDetails )) {
 		foreach ( $fitnessDetails as $fitness){
 			
 			if ( !empty( $fitness['center_name'] )){
 				$this->data['centerNameList'][$fitness['userGuid']]=$fitness['center_name'];
 			}
 		}
 	}
 	
 	$this->load->view('layout/header');
 	$this->load->view('registration/user', $this->data);
 	$this->load->view('layout/footer');
 }
 
 function add(){
 	if ( empty($_REQUEST['rid']) ){
 		redirect(base_url());
 	}
 	$sessionUserGuid	= $this->session->userdata['s_userguid'];

 	switch ($_REQUEST['rid']) {
 	
 		case PARTNER_ROLE_ID:{
 			
 			if (!empty($_REQUEST['email']) && !empty($_REQUEST['pass']) && !empty($_REQUEST['username']) && !empty($_REQUEST['centername']) && !empty($_REQUEST['phone']) ){
 				$result = $this->Registration_model->insertUserRegistrationDetails($_REQUEST, $sessionUserGuid);
 				$this->session->set_flashdata('message', $result['message']);
 				redirect(base_url().'index.php/registration/index?rid='.$_REQUEST['rid']);
 			}else{
 				$this->session->set_flashdata('message', 'please contact administrator');
 				redirect(base_url().'index.php/registration/index?rid='.$_REQUEST['rid'] );
 			}
 			
 			break;
 		}
 		case EMPLOYEE_ROLE_ID:{
 			
 			if (!empty($_REQUEST['email']) && !empty($_REQUEST['pass']) && !empty($_REQUEST['username']) && !empty($_REQUEST['phone']) ){
 				$result = $this->Registration_model->insertCustomerCareRegistrationDetails($_REQUEST, $sessionUserGuid);
 				
 				$this->session->set_flashdata('message', $result['message']);
 				redirect(base_url().'index.php/registration/customercare?rid='.$_REQUEST['rid']);
 			}else{
 				$this->session->set_flashdata('message', 'please contact administrator');
 				redirect(base_url().'index.php/registration/customercare?rid='.$_REQUEST['rid']);
 			}
 			
 			break;
 		}
 		default:{
 			redirect(base_url());
 			break;
 		}
 	}
 	
 }
 
 function checkuseremailavailabilty(){
 	
 	if ( isset( $_REQUEST['email'] ) && !empty( $_REQUEST['email'] )) {
 		$exists		= $this->User_model->userDetails( $_REQUEST['email'] );
 		$cusExists	= $this->User_model->customerDetails( $_REQUEST['email'] );
 		$msg	= 2;
 		if ( !empty( $exists ) || !empty( $cusExists )){
 			$msg	=	1;
 		}
 		echo  $msg;
 	}
 }
 
 function adduser(){
 	if ( empty( $_REQUEST )){
 		redirect(base_url());
 	}
 	$requestData = $_REQUEST;
 if (!empty($requestData['email']) && !empty($requestData['pass']) && !empty($requestData['username']) && !empty($requestData['phone']) && !empty($requestData['centername']) ){
 		$email	= strtolower($requestData['email']);
		$pass	= $requestData['pass'];
		$uname	= $requestData['username'];
		$phone	= $requestData['phone'];
		$gender		= (!empty( $requestData['gender'] ) ? $requestData['gender']:'others');
		$address	= (!empty( $requestData['address'] ) ? $requestData['address']:'');
		$centername	= $requestData['centername'];
			
		$sessionUserGuid	= $this->session->userdata['s_userguid'];
		$cusExists			= $this->User_model->customerDetails( $email );
		$userExists			= $this->User_model->userDetails( $email );
		if ( empty( $userExists ) && empty( $cusExists )) {
			$this->db->trans_begin();
			$userGuid  =   $this->Utils->getGuid();
			$data = array();
			if ( isset( $_FILES['photo']['error'] ) && $_FILES['photo']['error'] == 0) {
				$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/";
				$config['allowed_types'] = '*';
				// 	        $config['max_size'] = '100';
				// 	        $config['max_width']  = '1024';
				// 	        $config['max_height']  = '768';
				$config['file_name']  = $userGuid.'_'.strtotime(date(DATE_TIME_FORMAT));
	
				$this->load->library('upload', $config);
	
				if ( ! $this->upload->do_upload('photo')) {
					$error = array('error' => $this->upload->display_errors());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}
			}
			 
			$created		=	date(DATE_TIME_FORMAT);
			$insertUserData =   array(  'email'				=>	$email,
										'password'			=> 	md5($pass),
										'name'				=> 	$uname,
										'mobile'      		=>  $phone,
										'photo'      		=>  (!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:''),
										'gender'      		=>  $gender,
										'address'     		=>  $address,
										'created'      		=>  $created,
										'created_by'      	=>  $sessionUserGuid,
										'last_updated'     	=>  $created,
										'last_updated_by'   =>  $sessionUserGuid,
										'guid'				=>	$userGuid,
								);
			
			$insertUser	    =   $this->User_model->insertCustomerInfo($insertUserData);
	
			if ( $insertUser ) {
				$fittnessGuid  =   $this->Utils->getGuid();
				$insertFittnessData =   array(
												'customer_id' 		=>	$userGuid,
												'fitness_id' 		=>	$centername,
												'created'      		=>  $created,
												'created_by'      	=>  $sessionUserGuid,
												'last_updated'     	=>  $created,
												'last_updated_by'   =>  $sessionUserGuid,
												'guid'				=>	$fittnessGuid,
											);
				 
				$insertFittness        =   $this->User_model->insertFittenessInfo($insertFittnessData);
	
			}
	
			if (empty( $insertUser ) || empty( $insertFittness ) ) {
				$this->db->trans_rollback();
				$result['statuscode'] = 404;
				$result['status'] = 'Failure';
				$result['message'] = 'Registration Failed';
			}
			else {
				$result['statuscode'] = 200;
				$result['status'] = 'Success';
				$result['message'] = 'Registration Success';
				$this->db->trans_commit();
			}
			 
		}else{
			$result['statuscode'] = 404; 
			$result['status'] = 'Failure';
			$result['message'] = 'User Already exists with same email';
		}
	
	}else{
		$result['statuscode'] = 404;
		$result['status'] = 'Failure';
		$result['message'] = 'Request data empty';
	}
	$this->session->set_flashdata('message', $result['message']);
	redirect(base_url().'index.php/registration/user');
 }
 
 function checkcustomeremailavailabilty(){
 
 	if ( isset( $_REQUEST['email'] ) && !empty( $_REQUEST['email'] )) {
 		$exists		= $this->User_model->userDetails( $_REQUEST['email'] );
 		$cusExists	= $this->User_model->customerDetails( $_REQUEST['email'] );
 		$msg	= 2;
 		if ( !empty( $exists ) || !empty( $cusExists )){
 			$msg	=	1;
 		}
 		echo  $msg;
 	}
 }
 
}
 
?>