<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Logout extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->helper(array('url','language'));
   $this->load->model('User_model','',TRUE);
   $this->load->model('Login_model','',TRUE);
   $this->load->library('session');
    if ( !isset($this->session->userdata['s_roleguid']) && empty( $this->session->userdata['s_roleguid']) ){
   		redirect(base_url());
   }
 }
 
 function index() {
 	$data	=	array( 'session_id'=>'','s_email'=>'','s_roleName'=>'','s_userguid'=>'','s_roleguid'=>'','s_username'=>'','s_imagepath'=>'','fitnessId'=>'');
 	$this->session->unset_userdata($data);
 	$this->session->sess_destroy();
 	redirect(base_url());
 }
 
}
 
?>