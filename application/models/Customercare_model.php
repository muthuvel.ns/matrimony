<?php
Class Customercare_model extends CI_Model {
function __construct()
    {
        parent::__construct();
    }

 function insertMenu($data){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 	
 	$result = $this->db->insert('market_menu', $data);
 	return $result;
 	
 }
 
function updateMenu( $id, $data){
	$result	=	0;
	if ( empty($id) || empty($data) ) {
		return $result;
	}
	
	 				$this->db->where('id', $id);
	 $result	=	$this->db->update('market_menu', $data);
 	return $result;
 }
 
 function insertSubMenu($data){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 
 	$result = $this->db->insert('market_sub_menu', $data);
 	return $result;
 
 }
 
 function updateSubMenu( $id, $data){
 	$result	=	0;
 	if ( empty($id) || empty($data) ) {
 		return $result;
 	}
 
 	$this->db->where('id', $id);
 	$result	=	$this->db->update('market_sub_menu', $data);
 	return $result;
 }
 
function menuDetails() {
	$this -> db -> select('id, name');
		
	$query = $this -> db -> get('market_menu');
	return $query->result_array();
}

function subMenuDetails( $cid ='', $sid='', $roleId='') {
	$this -> db -> select('id, name, menu_id, role_id');
	if ( $roleId )	{
		$this -> db -> where('role_id', $roleId);
	}
	if ( $cid )	{
		$this -> db -> where('menu_id', $cid);
	}
	if ( $sid )	{
		$this -> db -> where('id', $sid);
	}
	$query = $this -> db -> get('market_sub_menu');
	return $query->result_array();
}

 
}
?>