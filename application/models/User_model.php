<?php
Class User_model extends CI_Model {
function __construct()
    {
        parent::__construct();
    }

 function insertUser($data){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 	
 	$result = $this->db->insert('user', $data);
 	return $result;
 	
 }
 
function insertUserProfile($data){
	$result	=	0;
	if ( empty($data) ) {
		return $result;
	}
	
	$result = $this->db->insert('user_profile', $data);
	return $result;
	
 }
 
function updateUser( $userGuid, $data){
	$result	=	0;
	if ( empty($userGuid) || empty($data) ) {
		return $result;
	}
	
	 				$this->db->where('guid', $userGuid);
	 $result	=	$this->db->update('user', $data);
 	return $result;
 }
 
 function insertCustomerInfo($data){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 
 	$result = $this->db->insert('customer_info', $data);
 	return $result;
 
 }
 
 function updateCustomerInfo( $userGuid, $data){
 	$result	=	0;
 	if ( empty($userGuid) || empty($data) ) {
 		return $result;
 	}
 
 	$this->db->where('guid', $userGuid);
 	$result	=	$this->db->update('customer_info', $data);
 	return $result;
 }
 
 function insertFittenessInfo($data){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 
 	$result = $this->db->insert('fittness_user_info', $data);
 	return $result;
 
 }
 
 function updateFittenessInfo( $userGuid, $data){
 	$result	=	0;
 	if ( empty($userGuid) || empty($data) ) {
 		return $result;
 	}
 
 	$this->db->where('guid', $userGuid);
 	$result	=	$this->db->update('fittness_user_info', $data);
 	return $result;
 }
 
 function updateUserProfile( $userGuid='', $data, $guid=''){
 	$result	=	0;
 	if ( empty($userGuid) && empty($data) || empty($data) && empty($guid) ) {
 		return $result;
 	}
 	if ( $userGuid ) {
 		$this->db->where('user_guid', $userGuid);
 	}
 	
 	if ( $guid ) {
 		$this->db->where('guid', $guid);
 	}
 	
 	$result	=	$this->db->update('user_profile', $data);
 	return $result;
 }
 
 function insertUserRole( $data ){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 
 	$result = $this->db->insert('user_role', $data);
 	return $result;
 }
 
 function updateUserRole( $userGuid='', $data, $guid=''){
 	$result	=	0;
 	if ( empty($userGuid) && empty($data) || empty($data) && empty($guid) ) {
 		return $result;
 	}
 	if ( $userGuid ) {
 		$this->db->where('user_guid', $userGuid);
 	}
 
 	if ( $guid ) {
 		$this->db->where('guid', $guid);
 	}
 
 	$result	=	$this->db->update('user_role', $data);
 	return $result;
 }

 function insertCustomerComplaints( $data ){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 
 	$result = $this->db->insert('customer_complaints', $data);
 	return $result;
 }
 
 function updateCustomerComplaints( $guid='', $data){
 	$result	=	0;
 	if (  empty($data)|| empty($guid) ) {
 		return $result;
 	}
 	if ( $guid ) {
 		$this->db->where('guid', $guid);
 	}
 
 	$result	=	$this->db->update('customer_complaints', $data);
 	return $result;
 }
 
 function insertPrivateLoginInfo($data){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 
 	$result = $this->db->insert('private_login_info', $data);
 	return $result;
 
 }
 
 function updatePrivateLoginInfo( $userGuid, $data){
 	$result	=	0;
 	if ( empty($userGuid) || empty($data) ) {
 		return $result;
 	}
 
 	$this->db->where('guid', $userGuid);
 	$result	=	$this->db->update('private_login_info', $data);
 	return $result;
 }
 
 function userDetails($email='', $guid='') {
 	$this -> db -> select('id, username, email, guid');
 	 
 	if( $email ){
 		$this -> db -> where('email', $email);
 	}
 	if( $guid ){
 		$this -> db -> where('guid', $guid);
 	}
 	$this -> db -> where('deleted', 0);
 	$query = $this -> db -> get('user');
 	return $query->result_array()  ;
 }
 
 function customerDetails($email='', $guid='') {
 	$this -> db -> select('guid AS userGuid, name, email, photo, mobile, address, gender ');
 		
 	if( $email ){
 		$this -> db -> where('email', $email);
 	}
 	if( $guid ){
 		$this -> db -> where('guid', $guid);
 	}
 	$this -> db -> where('deleted', 0);
 	$query = $this -> db -> get('customer_info');
 	return $query->result_array()  ;
 }
 
 function customerFitnessCenterDetails($guid='', $fitness_id='', $customer_id='') {
 	$this -> db -> select('*');
 		
 	if( $fitness_id ){
 		$this -> db -> where_in('fitness_id', $fitness_id);
 	}
 	if( $customer_id ){
 		$this -> db -> where_in('customer_id', $customer_id);
 	}
 	if( $guid ){
 		$this -> db -> where('guid', $guid);
 	}
 	$this -> db -> where('deleted', 0);
 	$query = $this -> db -> get('fittness_user_info');
 	return $query->result_array()  ;
 }
 
 function customerComplaintsDetails($fitnessId='', $guid='', $lastupdatedBy='', $lastupdated='', $groupby='') {
 	$this -> db -> select('*');
 		
 	if( $fitnessId ){
 		$this -> db -> where('fitness_id', $fitnessId);
 	}
 	if( $guid ){
 		$this -> db -> where('guid', $guid);
 	}
 	
 	if( $lastupdatedBy){
 		$this -> db -> where('last_updated_by', $lastupdatedBy);
 	}
 	if( $lastupdated){
 		$this -> db -> like('created', $lastupdated);
 	}
 	
 	if( $groupby ){
 		$this -> db -> group_by($groupby);
 	}
 	
 	$this -> db -> where('deleted', 0);
 	$this -> db -> order_by('last_updated','DESC');
 	$query = $this -> db -> get('customer_complaints');
//  	echo $this->db->last_query();exit;
 	return $query->result_array()  ;
 }
 
function userProfileDetails( $userGuid="", $guid=""){
	
	$this -> db -> select('user_guid, phone, photo, guid');
   
   if( $userGuid ){
        $this -> db ->where_in('user_guid', $userGuid);
   }
   if( $guid ){
   	$this -> db ->where_in('guid', $guid);
   }
 
   $this -> db ->where('deleted', 0);
   $query = $this -> db -> get('user_profile');
     
  return $query->result_array();
	
}

function getUserRoleInfo( $userGuid = ''){
	
	$this -> db -> select('role.name, ur.user_guid AS userGuid')
				->join('user_role as ur', 'role.guid = ur.role_guid');
	if( $userGuid ){
		$this -> db -> where('guid', $userGuid);
	}
	
	$query = $this -> db -> get('roles AS role');
	return $query->result_array()  ;
}

function getUserAndRoleDetails( $userGuid = '', $roleGuid='', $notInUserGuid ='', $email=''){

	$this -> db -> select('user.guid AS userGuid, user.username, user.email, role.name AS roleName,role.guid AS roleGuid')
				-> select('profile.phone, profile.photo, profile.guid AS profileGuid, profile.gender, profile.address, profile.web_url, profile.packages,profile.offers, profile.value_added_service, profile.center_name ')
				->join('user_profile AS profile', 'profile.user_guid = user.guid','left')
				->join('user_role as ur', 'ur.user_guid = user.guid')
				->join('roles AS role','role.guid = ur.role_guid');
	
	if( $userGuid ){
		$this -> db ->where_in('user.guid', $userGuid);
	}
	
	if( $email ){
		$this -> db ->where_in('user.email', $email);
	}
	
	if( $roleGuid ){
		$this -> db ->where_in('ur.role_guid', $roleGuid);
	}
	
	if( $notInUserGuid ){
		$this -> db ->where_not_in('user.guid', $notInUserGuid);
	}
	
	$this -> db ->where('user.deleted', 0);
	$query = $this -> db -> get('user');
	
	return $query->result_array()  ;
}


function countryDetails() {
	$this -> db -> select('id, name');
		
	$query = $this -> db -> get('countries');
	return $query->result_array();
}

function stateDetails( $cid ='') {
	$this -> db -> select('id, name');
	if ( $cid )	{
		$this -> db -> where('country_id', $cid);
	}
	$query = $this -> db -> get('states');
	return $query->result_array();
}

function cityDetails( $cid ='') {
	$this -> db -> select('id, name');
	if ( $cid )	{
		$this -> db -> where('state_id', $cid);
	}
	$query = $this -> db -> get('cities');
	return $query->result_array();
}
 
}
?>