<?php
Class Mail extends CI_Model
{

 function sendMail($to ='',$from ,$subject='',$message='',$attachment='',$cc='') {
 	if ( empty( $to ) ){
 		$to = 'noreply.kevellcorp@gmail.com';
 	}
 	// load Email Library
 	$ci = get_instance();
	$ci->load->library('email');
	$ci->email->initialize();
	$ci->email->from($to);
	$list = array( $from );
	$ci->email->to($list);
	$ci->email->subject($subject);
	$ci->email->message($message);
	
 	if($attachment == 1){
 		$ci->email->attach('invoice.pdf');
 	}
	if ( !$ci->email->send()) {
 		echo $ci->email->print_debugger();exit;
	}
	return  true;
 }
 
 
}
?>
