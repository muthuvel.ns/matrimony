<?php
Class Login_model extends CI_Model
{
function __construct()
    {
        parent::__construct();
    }

 function loginValidation( array $options ){
 	/** Variable creation */
 	$userDetail		=	array();
 		
 	/** Input validation */
 	if(empty($options['password']) || empty($options['username']))
 		return $userDetail;
 		
 		$password		=	md5($options['password']);
 			
 		/** Get user detail information based on email id  **/
 		$userDetail 	=	$this->getLoginUserCredentialDetail( $options );
 			
 		/** Check Password are match **/
 		if(!empty($userDetail) && $password ) {
 			if($userDetail['0']['password']!= $password ) {
 				$userDetail = array();
 			}
 		}
 		
 		return $userDetail;
 }
 
 function getLoginUserCredentialDetail( $options ){
 	$result = array();
 	$username		= (!empty($options['username']))? $options['username']:'';
 	if ( empty( $username )) {
 		return $result;
 	}
 	
 	$this -> db -> select('user.username, user.password, user.email, user.guid AS userGuid,userrole.role_guid AS userRoleGuid, role.name AS roleName')
 				->join('user_role AS userrole', 'userrole.user_guid=user.guid')
  				->join('roles AS role', 'role.guid=userrole.role_guid')
				-> where('user.email', $username);
 	$query = $this -> db -> get('user');
 	
 	return $query->result_array();
 }
 
 function loginRedirection( $userRoleGuid ){
 		/** Input validation  */
 		if(empty($userRoleGuid)){
 			return false;
 		}
 			
 		/** variable creation  */
 		$path	= '';
 			
 		switch ($userRoleGuid) {
 
 			case ADMIN_ROLE_ID:{
 				$path	= base_url().'index.php/home';
 				return $path;
 				break;
 			}
 			case EMPLOYEE_ROLE_ID:{
 				$path	= base_url().'index.php/customercare/';
 				return $path;
 				break;
 			}
 			default:{
 				$path = 'index.php/logout';
 				return $path;
 				break;
 			}
 		}
 }
 
 function loginActivityEntry( $userGuid, $logId=1, $deviceType='Cloud' ){
 	$result=0;
 	if ( empty( $userGuid )) {
 		return $result;
 	}
 	
 	$loginDate	=	date(DATE_TIME_FORMAT);
 	$activitydata	= array(
				 			'activity_log_id' 	=> $logId,
				 			'user_guid'			=> $userGuid,
				 			'client_date'		=> $loginDate,
				 			'activity_data1'	=> 'Login',
				 			'activity_data2' 	=> 'Login',
				 			'activity_comment' 	=> 'Logged in Successfully',
				 			'device_type' 		=> $deviceType,
				 			'created' 			=> $loginDate,
				 			'created_by' 		=> $userGuid,
 						);
 	$insert	=	$this->db->insert('activity_log', $activitydata);
 	return $insert;
 }
 
function getPrivateLoginInfo( $userGuid ){
 	$result = array();
 	if ( empty( $userGuid )) {
 		return $result;
 	}
 	
 	$this -> db -> select('user.username, user.password, user.email, user.guid AS userGuid, private.fitness_id')
 				->join('user_role AS userrole', 'userrole.user_guid=user.guid')
  				->join('private_login_info AS private', 'private.cutomercare_id=user.guid AND private.cust_role_id=userrole.role_guid')
				-> where('user.guid', $userGuid);
 	$query = $this -> db -> get('user');
 	
 	return $query->result_array();
 }
 
 
}
?>