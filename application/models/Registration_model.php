<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Registration_model extends CI_Model
{
function __construct()  {
        parent::__construct();
        $this->load->model('User_model','',TRUE);
        $this->load->model('Utils','',TRUE);
 }

function insertUserRegistrationDetails($requestData, $sessionUserGuid){
	$result = array();
	if (!empty($requestData['email']) && !empty($requestData['pass']) && !empty($requestData['username']) && !empty($requestData['phone']) && !empty($requestData['rid']) ){
		$email	= strtolower($requestData['email']);
		$pass	= $requestData['pass'];
		$uname	= $requestData['username'];
		$phone	= $requestData['phone'];
		$gender		= (!empty( $requestData['gender'] ) ? $requestData['gender']:'others');
		$address	= (!empty( $requestData['address'] ) ? $requestData['address']:'');
		$centername	= (!empty( $requestData['centername'] ) ? $requestData['centername']:'');
		$weburl		= (!empty( $requestData['web_url'] ) ? $requestData['web_url']:'');
		$package	= (!empty( $requestData['package'] ) ? $requestData['package']:'');
		$offer		= (!empty( $requestData['offer'] ) ? $requestData['offer']:'');
		$valueAdded	= (!empty( $requestData['value_added_service'] ) ? $requestData['value_added_service']:'');
			
		$userRoleGuid		= $requestData['rid'];
		$userExists	=	$this->User_model->userDetails( $email );
		if ( empty( $userExists )) {
			$this->db->trans_begin();
			$userGuid  =   $this->Utils->getGuid();
			$data = array();
			if ( isset( $_FILES['photo']['error'] ) && $_FILES['photo']['error'] == 0) {
				$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/";
				$config['allowed_types'] = '*';
				// 	        $config['max_size'] = '100';
				// 	        $config['max_width']  = '1024';
				// 	        $config['max_height']  = '768';
				$config['file_name']  = $userGuid.'_'.strtotime(date(DATE_TIME_FORMAT));
	
				$this->load->library('upload', $config);
	
				if ( ! $this->upload->do_upload('photo')) {
					$error = array('error' => $this->upload->display_errors());
					
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}
			}
			 
			$created		=	date(DATE_TIME_FORMAT);
			$insertUserData =   array(  'email'				=>	$email,
										'password'			=> 	md5($pass),
										'username'			=> 	$uname,
										'created'      		=>  $created,
										'created_by'      	=>  $sessionUserGuid,
										'last_updated'     	=>  $created,
										'last_updated_by'   =>  $sessionUserGuid,
										'guid'				=>	$userGuid,
								);
			
			$insertUser	    =   $this->User_model->insertUser($insertUserData);
	
			if ( $insertUser ) {
				$profileGuid  =   $this->Utils->getGuid();
				$insertProfileData =   array(
												'user_guid' 		=>	$userGuid,
												'phone' 			=>	$phone,
												'photo' 			=>	(!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:''),
												'address' 			=>	$address,
												'gender' 			=>	$gender,
												'center_name' 		=>	$centername,
												'web_url'      		=>  $weburl,
												'packages'      	=>  $package,
												'offers'      		=>  $offer,
												'value_added_service'=>  $valueAdded,
												'created'      		=>  $created,
												'created_by'      	=>  $sessionUserGuid,
												'last_updated'     	=>  $created,
												'last_updated_by'   =>  $sessionUserGuid,
												'guid'				=>	$profileGuid,
											);
				 
				$insertProfile        =   $this->User_model->insertUserProfile($insertProfileData);
	
				if ( !empty( $insertProfile )){
					 
					$roleGuid  =   $this->Utils->getGuid();
					$insertRoleData 	=   array(
													'user_guid' 		=>	$userGuid,
													'role_guid' 		=>	$userRoleGuid,
													'created'      		=>  $created,
													'created_by'      	=>  $sessionUserGuid,
													'last_updated'     	=>  $created,
													'last_updated_by'   =>  $sessionUserGuid,
													'guid'				=>	$roleGuid,
											);
	
					$insertuserRole        =   $this->User_model->insertUserRole( $insertRoleData );
				}
			}
			
			if ( $userRoleGuid == EMPLOYEE_ROLE_ID ) {
				
				$roleGuid  =   $this->Utils->getGuid();
					$insertRoleData 	=   array(
													'cutomercare_id' 	=>	$userGuid,
													'cust_role_id' 		=>	$userRoleGuid,
													'fitness_id' 		=>	$userRoleGuid,
													'created'      		=>  $created,
													'created_by'      	=>  $sessionUserGuid,
													'last_updated'     	=>  $created,
													'last_updated_by'   =>  $sessionUserGuid,
													'guid'				=>	$roleGuid,
											);
				$insertLogin        =   $this->User_model->insertPrivateLoginInfo( $insertPrivateData );
			}
	
			if (empty( $insertUser ) || empty( $insertProfile ) ||  empty( $insertuserRole ) ) {
				$this->db->trans_rollback();
				$result['statuscode'] = 404;
				$result['status'] = 'Failure';
				$result['message'] = 'Registration Failed';
			}
			else {
				$result['statuscode'] = 200;
				$result['status'] = 'Success';
				$result['message'] = 'Registration Success';
				$this->db->trans_commit();
			}
			 
		}else{
			$result['statuscode'] = 404; 
			$result['status'] = 'Failure';
			$result['message'] = 'User Already exists with same email';
		}
	
	}else{
		$result['statuscode'] = 404;
		$result['status'] = 'Failure';
		$result['message'] = 'Request data empty';
	}
	
	return $result;
}


function insertCustomerCareRegistrationDetails($requestData, $sessionUserGuid){
	$result = array();
	if (!empty($requestData['email']) && !empty($requestData['pass']) && !empty($requestData['username']) && !empty($requestData['phone']) && !empty($requestData['rid']) ){
		$email	= strtolower($requestData['email']);
		$pass	= $requestData['pass'];
		$uname	= $requestData['username'];
		$phone	= $requestData['phone'];
		$gender		= (!empty( $requestData['gender'] ) ? $requestData['gender']:'others');
		$address	= (!empty( $requestData['address'] ) ? $requestData['address']:'');
		$centername	= (!empty( $requestData['centername'] ) ? $requestData['centername']:'');
			
		$userRoleGuid		= $requestData['rid'];
		$userExists	=	$this->User_model->userDetails( $email );
		if ( empty( $userExists )) {
			$this->db->trans_begin();
			$userGuid  =   $this->Utils->getGuid();
			$data = array();
			if ( isset( $_FILES['photo']['error'] ) && $_FILES['photo']['error'] == 0) {
				$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/";
				$config['allowed_types'] = '*';
				// 	        $config['max_size'] = '100';
				// 	        $config['max_width']  = '1024';
				// 	        $config['max_height']  = '768';
				$config['file_name']  = $userGuid.'_'.strtotime(date(DATE_TIME_FORMAT));
	
				$this->load->library('upload', $config);
	
				if ( ! $this->upload->do_upload('photo')) {
					$error = array('error' => $this->upload->display_errors());
					
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}
			}
			 
			$created		=	date(DATE_TIME_FORMAT);
			$insertUserData =   array(  'email'				=>	$email,
										'password'			=> 	md5($pass),
										'username'			=> 	$uname,
										'created'      		=>  $created,
										'created_by'      	=>  $sessionUserGuid,
										'last_updated'     	=>  $created,
										'last_updated_by'   =>  $sessionUserGuid,
										'guid'				=>	$userGuid,
								);
			
			$insertUser	    =   $this->User_model->insertUser($insertUserData);
	
			if ( $insertUser ) {
				$profileGuid  =   $this->Utils->getGuid();
				$insertProfileData =   array(
												'user_guid' 		=>	$userGuid,
												'phone' 			=>	$phone,
												'photo' 			=>	(!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:''),
												'address' 			=>	$address,
												'gender' 			=>	$gender,
												'created'      		=>  $created,
												'created_by'      	=>  $sessionUserGuid,
												'last_updated'     	=>  $created,
												'last_updated_by'   =>  $sessionUserGuid,
												'guid'				=>	$profileGuid,
											);
				 
				$insertProfile        =   $this->User_model->insertUserProfile($insertProfileData);
	
				if ( !empty( $insertProfile )){
					 
					$roleGuid  =   $this->Utils->getGuid();
					$insertRoleData 	=   array(
													'user_guid' 		=>	$userGuid,
													'role_guid' 		=>	$userRoleGuid,
													'created'      		=>  $created,
													'created_by'      	=>  $sessionUserGuid,
													'last_updated'     	=>  $created,
													'last_updated_by'   =>  $sessionUserGuid,
													'guid'				=>	$roleGuid,
											);
	
					$insertuserRole        =   $this->User_model->insertUserRole( $insertRoleData );
				}
			}
			
			if ( $userRoleGuid == EMPLOYEE_ROLE_ID && !empty( $centername )) {
				
					$guid  =   $this->Utils->getGuid();
					$insertPrivateData 	=   array(
													'cutomercare_id' 	=>	$userGuid,
													'cust_role_id' 		=>	$userRoleGuid,
													'fitness_id' 		=>	$centername,
													'created'      		=>  $created,
													'created_by'      	=>  $sessionUserGuid,
													'last_updated'     	=>  $created,
													'last_updated_by'   =>  $sessionUserGuid,
													'guid'				=>	$guid,
											);
				$insertLogin        =   $this->User_model->insertPrivateLoginInfo( $insertPrivateData );
			}
	
			if (empty( $insertUser ) || empty( $insertProfile ) ||  empty( $insertuserRole ) ) {
				$this->db->trans_rollback();
				$result['statuscode'] = 404;
				$result['status'] = 'Failure';
				$result['message'] = 'Registration Failed';
			}
			else {
				$result['statuscode'] = 200;
				$result['status'] = 'Success';
				$result['message'] = 'Registration Success';
				$this->db->trans_commit();
			}
			 
		}else{
			$result['statuscode'] = 404; 
			$result['status'] = 'Failure';
			$result['message'] = 'User Already exists with same email';
		}
	
	}else{
		$result['statuscode'] = 404;
		$result['status'] = 'Failure';
		$result['message'] = 'Request data empty';
	}
	
	return $result;
}
 
}
?>